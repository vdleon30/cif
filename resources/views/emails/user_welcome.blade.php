<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title></title>
    </head>
    <body>
        <div style="border: 1px solid #eee; max-width: 600px; border-top: 0; border-bottom:0; margin: 0 auto">
            <div style="margin-bottom: 25px;height: 7px; width: 100%; background: #197ea0"></div>
            <div style="text-align: center; ">
                <a href="{{route('dashboard')}}"><img src="{{asset("/images/logo.png")}}" alt="Woove Logo"/></a>
            </div>
            <div style="margin: 10px 10px; text-align: left; padding: 10px; color: #555">
                <p>{{ $user->first_name }},</p>
                <p>¡Bienvenido al <b>Centro de Igeniería y Fabricación!</b></p>
                <p>Para ingresar a nuestra plataforma, haga click <a href="http://woove.789.com.mx">aquí</a>, con la siguiente información</p>
                <p>Usuario: {{ $user->email }}</p>
                <p>Contraseña: {{ $data["password"] }} (Cámbiela una vez ingrese) </p>
            </div>
            <div style="width: 100%; background: #197ea0; padding: 10px; box-sizing: border-box; color: #fff; text-align: center; font-size: 12px;"> 
                <a style="color: white; text-decoration:none;" href="#"><img src="{{asset("/images/facebook.png")}}"></a>
                <a style="color: white; text-decoration:none;" href="#"><img src="{{asset("/images/twitter.png")}}"></a>
                <a style="color: white; text-decoration:none;" href="#"><img src="{{asset("/images/instagram.png")}}"></a>
            </div>
        </div>
    </body>
</html>