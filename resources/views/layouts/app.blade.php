<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="{{asset("images/logo_body.png")}}" rel="icon" type="image/png">
    
    <link rel="stylesheet" href="{{mix("css/app.css")}}">
    <script src="{{asset("js/app.js")}}"></script>
  </head>
  <body >
    <style type="text/css">
       nav.menu a.active {
    color: black !important;
    background: linear-gradient(to right, #dcd5d5 0%, #3633bd 100%);
}
      #single-section .blog-entry {
    border: 1px solid #f5f7f9;
    background: #fff;
    box-shadow: 0px 3px 12px -3px rgba(0, 0, 0, 0.2);
}
#single-section .blog-entry:hover {
    background: #eeeeee47;
    box-shadow: 0px 3px 22px -3px rgba(0, 0, 0, 0.2);
    cursor: pointer;

}

      .ftco-section .alert{
        width: 90%;
        text-transform: capitalize;
        margin-bottom: 17rem;
      }

.content-area {
    width: 100%;
    text-align: center;
    margin: 0 auto;
    border-left: 0 !important;
}
.content-area {
    border: 0 !important;
}
 @media (min-width: 981px){
.et_left_sidebar:not(.et-fb) .content-area {
    border-left: 12px solid #40b4e5;
}
}
@media (min-width: 1100px){
.content-area {
    width: 50%;
}
}
@media (min-width: 981px){
.content-area {
    width: 60%;
}
}
.left-area {
    margin: 0 auto;
    width: 80% !important;
    padding: 25px 0 !important;
    float: none !important;
}
.left-area {
    margin: 0 auto;
    width: 80% !important;
    padding: 25px 0 !important;
    float: none !important;
}
@media (min-width: 981px)
.left-area {
    padding: 0 4% 0 8%;
}
@media (min-width: 981px)
.left-area {
    float: right;
    padding-left: 5.5%;
}
@media (min-width: 981px)
.left-area {
    width: 79.125%;
    padding-bottom: 23px;
}

.left-area .no-result-ucab-logo {
    width: 100px;
    height: 100px;
    margin: 0 auto 20px auto;
    background-size: contain;
    background-image: url(images/logo_body.png);
    filter: opacity(0.7)
}
 .entry h1 {
    font-size: 1.75em;
    color: #3331b3;
    text-transform: uppercase;
    font-weight: 600;
    filter: opacity(0.9);
}
.left-area a {
  float: right;
    display: inline-block;
    padding: 10px 15px;
    margin: 5px;
    line-height: 1;
    text-transform: uppercase;
    transition: all 0.15s ease-in-out;
    color: #3331b3;
    filter: opacity(0.9);
font-weight: 500;
}
.left-area a:hover{
  padding-right:  20px
}
.left-area a:hover span{

  padding-right: 1px

}
.left-area a  span{
      color: unset;
}
.ftco-section{
    padding-top: 5rem;
}
  .carousel-control-next-icon {
    background-size: 15px;
        background-color: black;
    width: 25px;
    height: 25px;
    border-radius: 50rem

}
.carousel-control-prev-icon {
    background-size: 15px;
        background-color: black;
    width: 25px;
    height: 25px;
    border-radius: 50rem

}
    </style>
 

 
 
 
    @include("includes.nav_bar")


    @yield('content')

    @include("includes.footer")

   
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <script src="{{asset("pluggin/jquery.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery-migrate-3.0.1.min.js")}}"></script>
  <script src="{{asset("pluggin/popper.min.js")}}"></script>
  <script src="{{asset("pluggin/bootstrap.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.easing.1.3.js")}}"></script>
  <script src="{{asset("pluggin/jquery.waypoints.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.stellar.min.js")}}"></script>
  <script src="{{asset("pluggin/owl.carousel.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.magnific-popup.min.js")}}"></script>
  <script src="{{asset("pluggin/aos.js")}}"></script>
  <script src="{{asset("pluggin/jquery.animateNumber.min.js")}}"></script>
  <script src="{{asset("pluggin/bootstrap-datepicker.js")}}"></script>
  <script src="{{asset("pluggin/jquery.timepicker.min.js")}}"></script>
  <script src="{{asset("pluggin/scrollax.min.js")}}"></script>
  <script src="{{asset("pluggin/main.js")}}"></script>
   <script type="text/javascript">
    var $els = $('.menu a, .menu header');
    var count = $els.length;
    var grouplength = Math.ceil(count/3);
    var groupNumber = 0;
    var i = 1;
    $('.menu').css('--count',count+'');
    $els.each(function(j){
        if ( i > grouplength ) {
            groupNumber++;
            i=1;
        }
        $(this).attr('data-group',groupNumber);
        i++;
    });

    $('.menu footer button').on('click',function(e){
        e.preventDefault();
        var top = 2
        $els.each(function(j){
            $(this).css('--top',$(this)[0].getBoundingClientRect().top + ($(this).attr('data-group') * -15) - 20 + top);
            $(this).css('--delay-in',j*.1+'s');
            $(this).css('--delay-out',(count-j)*.1+'s');
            top = top +110
        });
        $els.each(function(j){
            if($('.menu').hasClass('closed'))
                $(this).css('display','block');
            else
                $(this).css('display','none');
        });
        if($('.menu').hasClass('closed')){
            $("#side-bar").removeClass('col-lg-1').removeClass('col-md-2').removeClass('col-3').addClass('col-lg-3').addClass('col-md-4').addClass('col-10').css('position','relative')
            $("#content").removeClass('col-12').addClass('col-lg-9').addClass('col-md-8')
        }else{
            $("#side-bar").addClass('col-lg-1').addClass('col-md-2').addClass('col-3').removeClass('col-lg-3').removeClass('col-md-4').removeClass('col-10').css('position','absolute')
            $("#content").addClass('col-12').removeClass('col-lg-9').removeClass('col-md-8')
        }
        $('.menu').toggleClass('closed');
        e.stopPropagation();
    });

    $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip(); 
        $('.menu footer button').click()
    })
</script>
  @yield('scripts')

  </body>
</html>