<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset("images/logo_body.png")}}" rel="icon" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <style type="text/css">
        #app .card-header{
                background: linear-gradient(to right, #0c0c0c 0%, #3633bd 100%);
    color: white;
    text-transform: uppercase;
        }
    </style>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel" id="nav-bar-auth">
            <div class="container">
                <a class="navbar-brand  ftco-animate" href="{{route("dashboard")}}" style="width: 14rem;">
                    <img src="{{asset("/images/logo.png")}}" style="width: inherit;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse ftco-animate" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a>
                            </li>
                            
                        @else
                            <li class="nav-item dropdown">
                                    
                                 
                                <a id="navbarDropdown" class="nav-link " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="fa fa-world"></span> Notifications <span class="badge"> {{ count(auth()->user()->unreadNotifications) }}</span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @foreach(auth()->user()->unreadNotifications as $notifications)
                                        <a class="dropdown-item">
                                          {{ snake_case(class_basename($notifications->type)) }}
                                        </a>
                                    @endforeach
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
     <script src="{{asset("pluggin/jquery.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery-migrate-3.0.1.min.js")}}"></script>
  <script src="{{asset("pluggin/popper.min.js")}}"></script>
  <script src="{{asset("pluggin/bootstrap.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.easing.1.3.js")}}"></script>
  <script src="{{asset("pluggin/jquery.waypoints.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.stellar.min.js")}}"></script>
  <script src="{{asset("pluggin/owl.carousel.min.js")}}"></script>
  <script src="{{asset("pluggin/jquery.magnific-popup.min.js")}}"></script>
  <script src="{{asset("pluggin/aos.js")}}"></script>
  <script src="{{asset("pluggin/jquery.animateNumber.min.js")}}"></script>
  <script src="{{asset("pluggin/bootstrap-datepicker.js")}}"></script>
  <script src="{{asset("pluggin/jquery.timepicker.min.js")}}"></script>
  <script src="{{asset("pluggin/scrollax.min.js")}}"></script>
  <script src="{{asset("pluggin/main.js")}}"></script>
</body>
</html>
