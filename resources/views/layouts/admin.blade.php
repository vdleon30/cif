<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset("images/logo_body.png")}}" rel="icon" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/admin.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('pluggin/dropzone/dist/dropzone.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    .custom-shadow {
    box-shadow: 0px 5px 13px -8px black !important;
}
</style>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        @include("includes.admin.admin_menu")
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                @include("includes.admin.admin_nav_bar")
                @yield('content')
            </div>
            <!-- End of Main Content -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    @include("includes.notify")
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
