<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <style type="text/css">
        .navbar-nav.toggled .sidebar-brand img{
            width: 5rem;

        }
        .navbar-nav .sidebar-brand img{
            width: 12rem;
        }
    </style>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route("admin.dashboard")}}">
        <img src="{{asset("/images/logo_blanco.png")}}" >
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item  {{isset($menu_active) && $menu_active=="dashboard"?"active":""}}">
        <a class="nav-link" href="{{route("admin.dashboard")}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Inicio</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Usuarios
    </div>
    @role('admin')
    <!-- Nav Item - Pages Collapse Menu fas fa-fw fa-cog-->
    <li class="nav-item {{isset($menu_active) && $menu_active=="users"?"active":""}}">
        <a class="nav-link" href="{{route("admin.users")}}">
            <i class="fas fa-fw fa-user"></i>
            <span>Usuarios</span>
        </a>
    </li>
    @endrole
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item {{isset($menu_active) && $menu_active=="personal"?"active":""}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-user-tie"></i>
            <span>Personal</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Personal:</h6>
                <a class="collapse-item {{isset($sub_menu_active) && $sub_menu_active=="personal_list"?"active":""}}" href="{{route("admin.personal")}}">Lista</a>
                <h6 class="collapse-header">Extras:</h6>
                <a class="collapse-item {{isset($sub_menu_active) && $sub_menu_active=="job_list"?"active":""}}" href="{{route("admin.personal.job")}}">Cargo</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Otros
    </div>
    @role('admin')

    <li class="nav-item {{isset($menu_active) && $menu_active=="customize"?"active":""}}">
        <a class="nav-link" href="{{route("admin.customize")}}">
            <i class="fas fa-fw fa-palette"></i>
            <span>Personalizar</span>
        </a>
    </li>
    @endrole

    <li class="nav-item {{isset($menu_active) && $menu_active=="publication"?"active":""}}">
        <a class="nav-link" href="{{route("admin.publication")}}">
            <i class="fas fa-fw fa-book"></i>
            <span>Publicaciones</span>
        </a>
    </li>
    <li class="nav-item {{isset($menu_active) && $menu_active=="events"?"active":""}}">
        <a class="nav-link" href="{{route("admin.events")}}">
            <i class="fas fa-fw fa-calendar-alt"></i>
            <span>Eventos</span>
        </a>
    </li>

    <li class="nav-item {{isset($menu_active) && $menu_active=="news"?"active":""}}">
        <a class="nav-link" href="{{route("admin.news")}}">
            <i class="far fa-fw fa-newspaper"></i>
            <span>Noticias</span>
        </a>
    </li>
    <li class="nav-item {{isset($menu_active) && $menu_active=="gallery"?"active":""}}">
        <a class="nav-link" href="{{route("admin.gallery")}}">
            <i class="fas fa-images"></i>
            <span>Galería</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->