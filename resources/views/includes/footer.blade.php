    <footer class="ftco-footer ftco-bg-dark ftco-section img" style="background-image: url(images/bg_2.jpg); background-attachment:fixed;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <a class="navbar-brand" href="#">
                            <img src="{{asset("/images/logo_blanco.png")}}" style="width: 17rem;">
                        </a>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            @if ($platform->twitter)
                                <li class="ftco-animate"><a target="_blank" href="{{$platform->twitter}}"><span class="icon-twitter"></span></a></li>
                            @endif
                            @if ($platform->facebook)
                             <li class="ftco-animate"><a target="_blank" href="{{$platform->facebook}}"><span class="icon-facebook"></span></a></li>
                            @endif
                            @if ($platform->instagram)
                                <li class="ftco-animate"><a target="_blank" href="{{$platform->instagram}}"><span class="icon-instagram"></span></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Recent Blog</h2>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url(images/image_1.jpg);"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> July 12, 2018</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url(images/image_2.jpg);"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> July 12, 2018</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">Site Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">Home</a></li>
                            <li><a href="#" class="py-2 d-block">About</a></li>
                            <li><a href="#" class="py-2 d-block">Courses</a></li>
                            <li><a href="#" class="py-2 d-block">Students</a></li>
                            <li><a href="#" class="py-2 d-block">Video</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Tienes preguntas?</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">{{$platform->address}}</span></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">{{$platform->phone}}</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">{{$platform->email}}</span></a></li>
                                <hr style="background-color: white">
                                <li><a href="{{ route('login') }}"><span class="icon icon-lock"></span><span class="text">Administrador</span></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>