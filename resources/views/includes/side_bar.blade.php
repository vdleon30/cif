<nav class="menu">
    <header>MENÚ<span>×</span></header>
    <ol>
        {{--<li class="menu-item"><a href="{{ route('contact_us') }}">Investigaciones</a></li>
        <li class="menu-item"><a href="{{ route('contact_us') }}">Innovación</a></li>
        <li class="menu-item"><a href="{{ route('contact_us') }}">Instalaciones</a></li>
        <li class="menu-item"><a href="#">Colaboradores</a></li>--}}
        <li class="menu-item"><a href="{{ route('about_us.type','enlaces_de_interes') }}" class="{{isset($sub_menu_active) && $sub_menu_active == "link"?"active":""}}">Enlaces de Interes</a></li>
        <li class="menu-item"><a href="{{ route('contact_us') }}" class="{{isset($sub_menu_active) && $sub_menu_active == "contact_us"?"active":""}}">Contáctanos</a></li>
        <!--<li class="menu-item">
            <a href="#0">Widgets</a>
            <ol class="sub-menu">
                <li class="menu-item"><a href="#0">Big Widgets</a></li>
                <li class="menu-item"><a href="#0">Bigger Widgets</a></li>
                <li class="menu-item"><a href="#0">Huge Widgets</a></li>
            </ol>
        </li>-->
    </ol>
    <footer><button class="btn btn-outline-light" aria-label="Toggle Menu"><span class="oi oi-menu"></span></button></footer>
</nav> 