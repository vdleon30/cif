<nav class="navbar navbar-expand-lg ftco-navbar-light" id="ftco-navbar">
    <div class="container-fluid">
        <a class="navbar-brand  ftco-animate" href="{{route("dashboard")}}" style="width: 14rem;">
            <img src="{{asset("/images/logo.png")}}" style="width: inherit;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-center " id="ftco-nav">
            <ul class="navbar-nav ml-auto mr-auto  ftco-animate">
                <li id="publications" class="nav-item {{isset($menu_active) && $menu_active=="publications"?"active ":""}}"><a href="{{ route('publications') }}" class="nav-link">Publicaciones</a></li>
                <li id="events" class="nav-item {{isset($menu_active) && $menu_active=="events"?"active ":""}}"><a href="{{ route('events') }}" class="nav-link">Eventos</a></li>
                <li id="news" class="nav-item {{isset($menu_active) && $menu_active=="news"?"active ":""}}"><a href="{{ route('news') }}" class="nav-link">Noticias</a></li>
                <li id="personal" class="nav-item {{isset($menu_active) && $menu_active=="personal"?"active ":""}}"><a href="{{ route('personal') }}" class="nav-link">Personal</a></li>
                <li id="galery" class="nav-item {{isset($menu_active) && $menu_active=="gallerys"?"active ":""}}"><a href="{{ route('gallery') }}" class="nav-link">Galería</a></li>
                <li id="about_us" class="nav-item {{isset($menu_active) && $menu_active=="about_us"?"active ":""}}"><a href="{{ route('about_us') }}" class="nav-link">Conócenos</a></li>
            </ul>
            <ul class="navbar-nav ml-auto  ftco-animate">
                <a class="navbar-brand" href="http://www.unexpo.edu.ve/">
                    <img src="{{asset("/images/logo_unexpo.png")}}" style="width: 4.5rem">
                </a>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->
