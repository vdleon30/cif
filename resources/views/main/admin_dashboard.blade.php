@extends('layouts.admin')

@section('content')
<div class="container-fluid">


          <div class="row mt-5">

            <div class="col-12">
              <div class="card shadow mb-4">
                <div class="card-body text-center my-5">
                  <h1>¡Bienvenido al Administrador del CIF!</h1>
                  <h3>Centro de ingenieria y fabricación</h3>
                  <br>
                   <img height="200" style="filter: opacity(0.6);" src="{{asset("/images/logo_body.png")}}" >
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
