@extends('layouts.app')

@section('content')

<style type="text/css">
   
</style>
<!-- Modal -->
<div class="modal fade" id="info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Confirmar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="body-modal">
                    ¿Está seguro que desea aceptar la solicitud?
                </div>
            </div>
        </div>
    </div>


<!-- End Modal -->
    <div class="hero-wrap" style="background-image: url('images/bg_1.jpg'); background-attachment:fixed;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
                <div class="col-md-8 ftco-animate text-center">
                    <h1 class="mb-4">Centro De Ingeniería De Fabricación</h1>
                    <!--<p>
                        <a href="{{route("login")}}" class="btn btn-primary px-4 py-3">Ingresa</a> 
                        <a href="{{route("register")}}" class="btn btn-secondary px-4 py-3">Regístrate</a>
                    </p>-->
                </div>
            </div>
        </div>
    </div>
    <section class="ftco-section">
        <div class="container">
            <div class="row basic-info">
                <div class="col-md-3 d-flex align-self-stretch ftco-animate justify-content-center">
                    <div class="media block-6 services p-3 py-4 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center mb-3" title="Misión" data-toggle="popover" data-trigger="hover" data-html="true" data-content="El centro de ingeniería de fabricación de la sección de tecnología de la UNEXPO vicerrectorado de puerto Ordaz, tiene como misión, desarrollar con carácter de excelencia, actividades de...">
                            <center>
                                <a href="{{ route('about_us') }}#mision">
                                    <img class="mision" src="{{asset('images/mision2.png')}}" data-toggle="tooltip" data-html="true" title="<b>Click</b><br> para ver más.">
                                </a>
                            </center>
                        </div>
                        <div class="media-body px-3">
                            <h3 class="heading">Misión</h3>
                        </div>
                    </div>      
                </div>
                <div class="col-md-3 d-flex align-self-stretch ftco-animate justify-content-center">
                    <div class="media block-6 services p-3 py-4 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center mb-3" title="Visión" data-toggle="popover" data-trigger="hover" data-html="true" data-content="Superar con excelencia científica y técnica y una capacidad de respuesta rápida las necesidades de desarrollo científico-técnico y los problemas tecnológicos de la industria...">
                            <center>
                                <a href="{{ route('about_us') }}#vision">
                                    <img src="{{asset('images/vision2.png')}}" data-toggle="tooltip" data-html="true" title="<b>Click</b><br> para ver más.">
                                </a>
                            </center>
                        </div>
                        <div class="media-body px-3">
                            <h3 class="heading">Visión</h3>
                        </div>
                    </div>      
                </div>
                <div class="col-md-3 d-flex align-self-stretch ftco-animate justify-content-center">
                    <div class="media block-6 services p-3 py-4 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center mb-3" title="Metas" data-toggle="popover" data-trigger="hover" data-html="true" data-content="•Sensibilizar y motivar a profesores y estudiantes para que participen de manera activa, en la solución de problemas relevantes de desarrollo regional y nacional.<br>•Establecer la integración e intercambio entre los investigadores...">
                            <center>
                                <a href="{{ route('about_us') }}#metas">
                                    <img src="{{asset('images/metas2.png')}}" data-toggle="tooltip" data-html="true" title="<b>Click</b><br> para ver más.">
                                </a>
                            </center>
                        </div>
                        <div class="media-body px-3">
                            <h3 class="heading">Metas</h3>
                        </div>
                    </div>    
                </div>
                <div class="col-md-3 d-flex align-self-stretch ftco-animate justify-content-center">
                    <div class="media block-6 services p-3 py-4 d-block text-center">
                        <div class="icon d-flex justify-content-center align-items-center mb-3" title="Objetivos" data-toggle="popover" data-trigger="hover" data-html="true" data-content="•Realizar estudios e investigaciones que permitan identificar las necesidades o problemas tecnológicos a que se enfrentan las empresas y ayudar a resolverlos.<br>•Proyectar líneas de producción, sistemas tecnológicos de talleres...">
                             <center>
                                <a href="{{ route('about_us') }}#objetivos">
                                    <img src="{{asset('images/objetivos2.png')}}" data-toggle="tooltip" data-html="true" title="<b>Click</b><br> para ver más.">
                                </a>
                            </center>
                        </div>
                        <div class="media-body px-3">
                            <h3 class="heading">Objetivos</h3>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>
    @if ($events->isNotEmpty())
        <section class="ftco-section  testimony-section bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7 heading-section ftco-animate text-center">
                        <h2 class="mb-4"><a href="{{route("events")}}">Eventos</a></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ftco-animate">
                        <div class="carousel-testimony owl-carousel justify-content-center">
                            @foreach($events as $key => $event)
                                @if($key%2==0)
                                    <div class="item">
                                       <div class="blog-entry align-self-stretch flex-column-reverse">
                                            @if (strpos($event->photo, 'pdf'))
                                                <center>
                                                    <a  class="block-20" style="background-image:url('/images/pdf.png');    width: 13rem;">
                                                    </a>
                                                </center>
                                            @else
                                                <a class="block-20" style="background-image: url('assets/files/events/img/{{$event->photo}}'); ">
                                                </a>
                                            @endif
                                            <div class="text p-4 d-block">
                                                <div class="meta mb-3">
                                                    <div><a href="#">{{$event->date}}</a></div>
                                                    <div><a>Admin</a></div>
                                                </div>
                                                <h3 class="heading"><a href="#">{{$event->title}}</a></h3>
                                                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> {!!\DateTime::createFromFormat("H:i",$event->hour)->format('g:ia')!!}</span> <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                                <p>{!!$event->description!!}</p>
                                                <p><a href="event.html">Ver Evento <i class="ion-ios-arrow-forward"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="item">
                                        <div class="blog-entry d-flex align-self-stretch flex-column-reverse">
                                            @if (strpos($event->photo, 'pdf'))
                                                <center>
                                                    <a  class="block-20" style="background-image:url('/images/pdf.png');    width: 13rem;">
                                                    </a>
                                                </center>
                                            @else
                                                <a class="block-20" style="background-image: url('assets/files/events/img/{{$event->photo}}'); ">
                                                </a>
                                            @endif
                                            <div class="text p-4 d-block">
                                                <div class="meta mb-3">
                                                    <div><a href="#">{{$event->date}}</a></div>
                                                    <div><a>Admin</a></div>
                                                </div>
                                                <h3 class="heading"><a href="#">{{$event->title}}</a></h3>
                                                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> {!!\DateTime::createFromFormat("H:i",$event->hour)->format('g:ia')!!}</span> <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                                <p>{!!$event->description!!}</p>
                                                <p><a href="event.html">Ver Evento <i class="ion-ios-arrow-forward"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
     


    <section class="ftco-section-3 img" style="background-image: url(images/bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex justify-content-center">
                <div class="col-md-9 about-video text-center">
                    <h2 class="ftco-animate">Genius University is a Leading Schools Around the World</h2>
                    <!--<div class="video d-flex justify-content-center">
                        <a href="https://vimeo.com/45830194" class="button popup-vimeo d-flex justify-content-center align-items-center"><span class="ion-ios-play"></span></a>
                    </div>-->
                </div>
            </div>
        </div>
    </section>

    @if ($news->isNotEmpty())
        <section class="ftco-section testimony-section bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7 heading-section ftco-animate text-center">
                        <h2 class="mb-4"><a href="{{route("news")}}">Noticias</a></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ftco-animate">
                        <div class="carousel-testimony owl-carousel justify-content-center">
                            @foreach($news as $key => $new)
                                <div class="item">
                                    <div class="blog-entry align-self-stretch flex-column-reverse">
                                        @if (strpos($new->photo, 'pdf'))
                                            <center>
                                                <a  class="block-20" style="background-image:url('/images/pdf.png');    width: 13rem;">
                                                </a>
                                            </center>
                                        @else
                                            <a class="block-20" style="background-image: url('assets/files/news/img/{{$new->photo}}'); ">
                                            </a>
                                        @endif
                                        <div class="text p-4 d-block">
                                            <div class="meta mb-3">
                                                <div><a>{{$new->date}}</a></div>
                                                <div><a>Admin</a></div>
                                            </div>
                                            <h3 class="heading mt-3"><a href="#">{{$new->title}}</a></h3>
                                            <p>{!!$new->description!!}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!--<section class="ftco-freeTrial">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center">
                        <div class="free-trial ftco-animate">
                            <h3>Try our free trial course</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
                        </div>
                        <div class="btn-join ftco-animate">
                            <p><a href="#" class="btn btn-primary py-3 px-4">Join now!</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->


    
   
   
@endsection

@section('scripts')
    
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    </script>

@endsection