<div class="et_pb_section et_pb_section_1 et_pb_fullwidth_section et_section_regular pt-4">
				<section class="et_pb_module et_pb_fullwidth_header et_pb_fullwidth_header_0 et_pb_bg_layout_light et_pb_text_align_center">
					<div class="et_pb_fullwidth_header_container center">
						<div class="header-content-container center ftco-animate">
							<div class="header-content">
								<h1 class="et_pb_module_header ftco-animate">Centro de Ingeniería y Fabricación</h1>
								<div class="et_pb_header_content_wrapper ftco-animate">
									<p>
										{!!$description!!}
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="et_pb_fullwidth_header_overlay"></div>
					<div class="et_pb_fullwidth_header_scroll"></div>
				</section>
			</div>
			<div class="et_pb_section et_pb_section_2 et_section_regular">
				<div class="et_pb_row et_pb_row_0">
					<div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
						@foreach ($gestion as $element)
							<div class="et_pb_module et_pb_cif_horizontal_blurb et_pb_cif_horizontal_blurb_0 " id="mision">
								<div class="et_pb_module_inner">
									<div class="et_pb_blurb et_pb_module et_pb_cif_horizontal_blurb et_pb_cif_horizontal_blurb_0  et_pb_blurb_url">
										<div class="et_pb_blurb_content">
											<div class="et_pb_blurb_container">
												<h4 class="et_pb_module_header ftco-animate"><a >{{$element->data_x}}</a></h4><hr>
												<div class="et_pb_blurb_description ftco-animate">
													<p>{!!$element->data_y!!}</p>
												</div><!-- .et_pb_blurb_description -->
											</div>
										</div> <!-- .et_pb_blurb_content -->
									</div> <!-- .et_pb_blurb -->
								</div>
							</div>
						@endforeach
					</div> <!-- .et_pb_column -->
				</div> <!-- .et_pb_row -->
			</div>