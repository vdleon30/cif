<div class="container">
	@foreach ($items as $element)
		@if ($element->links && $element->links->isNotEmpty())
			<h2 class="text-muted">{{$element->data_x}}</h2>
			<ul>
				@foreach ($element->links as $link)
					<li>{{ucfirst($link->data_x)}}: <a href="{{$link->data_y}}" target="_balnk">{{$link->data_y}}</a></li>
				@endforeach
			</ul>
		@endif
	@endforeach
</div>
