<div class="container">
    <div class="row d-flex mb-5 contact-info text-center">
        <div class="col-md-4">
            <p><span>Dirección: </span> {{$platform->address}} </p>
        </div>
        <div class="col-md-4">
            <p><span>Teléfono: </span>{{$platform->phone}}</p>
        </div>
        <div class="col-md-4">
            <p><span>Correo Electrónico: </span>{{$platform->email}}</p>
        </div>
    </div>
    <div class="row block-9">
        <div class="col-md-6 pr-md-5">
            <h4 class="mb-4">Tienes Alguna Pregunta?</h4>
            <form action="#">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nombre y Apellido">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Correo Electrónico">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Asunto">
                </div>
                <div class="form-group">
                    <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Mensaje"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
        <div class="col-md-6" id="map">
            <a href="https://www.google.com/maps/place/UNEXPO+-+Universidad+Nacional+Experimental+Polit%C3%A9cnica+Antonio+Jos%C3%A9+de+Sucre/@8.2796999,-62.7299594,15z/data=!4m5!3m4!1s0x0:0x843d9facb2b07ff5!8m2!3d8.2796999!4d-62.7299594" target="_blank">
                <img src="{{asset('images/address.jpg')}}">
            </a>
        </div>
    </div>
</div>