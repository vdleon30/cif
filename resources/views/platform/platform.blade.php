@extends('layouts.app')

@section('content')


<section class="ftco-section-parallax">
	<div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
					@if (isset($sub_menu_active) && $sub_menu_active == "link")
						<h2>Enalces de Interés</h2>
					@elseif(isset($sub_menu_active) && $sub_menu_active == "contact_us")
						<h2>Contáctanos</h2>

					@else
						<h2>Conócemos</h2>
						
					@endif
				</div>
			</div>
		</div>
	</div>
</section>

<div class="container-fluid">
	<div class="row">
		<div id="side-bar" class="col-3 col-md-4 col-lg-3 ftco-animate" style="z-index: 2">
			@include('includes.side_bar')
		</div>
		<div id="content" class="col-12 col-md-8 col-lg-9 ftco-animate" style="z-index: 1">
			@if ($sub_menu_active == "about_us_info")
				@include('platform.include.about_us_info')
			@elseif($sub_menu_active == "contact_us")
				@include('platform.include.contact_us')
			@elseif($sub_menu_active == "link")
				@include('platform.include.link')
			@endif
			
		</div>
	</div>
</div>
<section class="ftco-section contact-section ftco-degree-bg" style="padding-top: 2em">
</section>
@endsection
