@extends('layouts.app')

@section('content')
 <section id="events" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Eventos</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt-4">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-lg-3">
                <form id="form-filter" action="{{ route('events') }}" method="GET">
                    <input type="hidden" name="year">
                </form>
                <select id="select-year" class="form-control" placeholder="Category" required="">
                    @for ($i = \DateTime::createFromFormat("U",strtotime('this year'))->format("Y"); $i >= 2001 ; $i--)
                        <option {{$year == $i?"selected":""}} value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    @if ($events->isNotEmpty())
        <section class="ftco-section">
            <div class="container">
                <div class="row">
                    @foreach($events as $key => $event)
                    <div class="modal fade" id="open-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable {{strpos($event->photo, 'pdf')?'modal-lg':''}} " role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="openTitle">{{$event->title}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <center>
                                @if (strpos($event->photo, "pdf"))
                                    <div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="{{ asset('assets/files/events/img/'.$event->photo) }}"></iframe></div>
                                @else
                                    <a href="{{ asset('assets/files/events/img/'.$event->photo) }}" target="_blank" data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click</b> para ver la foto.">
                                        <img class="block-20" src="{{ asset('assets/files/events/img/'.$event->photo) }} ">
                                    </a>
                                @endif
                                
                            </center>
                            <hr>
                            {!!$event->description!!}
                          </div>
                        </div>
                      </div>
                    </div>
                    @if($key%2==0)
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch flex-column-reverse" data-toggle="modal" data-target="#open-{{$key}}" style="width: -webkit-fill-available;">
                            @if (strpos($event->photo, 'pdf'))
                                <center>
                                    <a href="#" data-toggle="modal" data-target="#open-{{$key}}" class="block-20" style="background-image:url('/images/pdf.png');    width: 13rem;">
                                     </a>
                                </center>
                                
                            @else
                                <a href="#" data-toggle="modal" data-target="#open-{{$key}}" class="block-20" style="background-image: url('assets/files/events/img/{{$event->photo}}'); ">
                                </a>
                            @endif

                            <div class="text p-4 d-block" data-toggle="modal" data-target="#open-{{$key}}">
                                <div class="meta mb-3">
                                    <div><a href="#">{{$event->date}}</a></div>
                                    <div><a href="#">Admin</a></div>
                                </div>
                                <h3 class="heading"><a href="#">{{$event->title}}</a></h3>
                                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> {!!\DateTime::createFromFormat("H:i",$event->hour)->format('g:ia')!!}</span> <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>{!!$event->description!!}</p>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="col-md-4 d-flex ftco-animate" data-toggle="modal" data-target="#open-{{$key}}">
                        <div class="blog-entry d-flex align-self-stretch flex-column-reverse" style="width: -webkit-fill-available;">
                            <a href="#" data-toggle="modal" data-target="#open-{{$key}}" class="block-20" style="background-image: url('assets/files/events/img/{{$event->photo}}');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">{{$event->date}}</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading"><a href="#">{{$event->title}}</a></h3>
                                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> {!!\DateTime::createFromFormat("H:i",$event->hour)->format('g:ia')!!}</span> <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>{!!$event->description!!}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                @if($events->total() > 0  && $events->lastPage() > 1)
                <div class="row mt-5">
                    <div class="col text-center">
                        <div class="block-27">
                            <ul>
                                <li class="{{$events->currentPage()==1?"page-item--disabled":""}}"><a href="{{route("events",["year"=>$year,"page"=>$events->currentPage()-1])}}">&lt;</a></li>
                                @for($i=1;$i<=$events->lastPage();$i++)
                                @if($events->currentPage()>=9 && $i==2)
                                <li class="">
                                    <a class="">&#8226;&#8226;&#8226;</a>
                                </li>
                                @endif
                                @if($events->currentPage()>=9 && $i>=$events->currentPage() && $i<$events->currentPage()+8)
                                <li class=" {{$events->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($events->currentPage()>=9 && $i==1)
                                <li class="">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($events->currentPage()>=9 && $i>=$events->currentPage()+8 && $events->lastPage() >= $events->currentPage()+9 && $i == $events->lastPage())
                                <li class="">
                                    <a class="">&#8226;&#8226;&#8226;</a>
                                </li>
                                <li class="{{$events->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$events->lastPage()])}}">{{$events->lastPage()}}</a>
                                </li>
                                @continue
                                @elseif($events->currentPage()>=9 && $i>=$events->currentPage()+8 && $events->lastPage() == $events->currentPage()+9 && $i == $events->lastPage())
                                <li class="{{$events->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$events->lastPage()])}}">{{$events->lastPage()}}</a>
                                </li>
                                @continue
                                @elseif($events->currentPage()>=9 && $events->lastPage() < $events->currentPage()+9 && $i>= $events->lastPage()-9)
                                <li class="">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($events->currentPage()>=9)
                                @continue
                                @endif
                                @if($events->currentPage()<9 && $i==10)
                                <li class="">
                                    <a class="">&gt;;&#8226;&#8226;</a>
                                </li>
                                <li class="{{$events->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$events->lastPage()])}}">{{$events->lastPage()}}</a>
                                </li>

                                @break
                                @endif
                                <li class="{{$events->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                </li>
                                @endfor
                                <li class="{{$events->currentPage()==$events->lastPage()?"page-item--disabled":""}}"><a href="{{route("events",["year"=>$year,"page"=>$events->currentPage()+1])}}">&gt;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
    @else
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="content-area clearfix">
                        <div class="left-area">
                            <article class="entry not_found">
                                <div class="no-result-ucab-logo"></div>
                                <h1>No se encontraron eventos</h1>
                                <p><a href="javascript:history.back()" ><span><i class="fas fa-arrow-left"></i></span> Regresar</a></p>
                            </article> <!-- .et_pb_post -->         
                        </div> <!-- #left-area -->
                            <div id="sidebar">
                        </div> <!-- end #sidebar -->
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
            $("select[id=select-year]").change(function(){
                $('input[name=year]').val($(this).val());
                $('input[name=page]').val(1)
                document.getElementById('form-filter').submit()
            });
        });
    </script>
@endsection