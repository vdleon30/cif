@extends('layouts.app')

@section('content')
 <section id="galery" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Galería</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($gallerys->isNotEmpty())
    <div class="modal fade" id="open" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="openTitle">Galería</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                              </div>
                            </div>
                          </div>
                        </div>
        <section class="ftco-section">
            <div class="container">
                <div class="row">
                    @foreach($gallerys as $key => $event)
                        
                        <div class="col-md-4 d-flex ftco-animate" >
                            <div class="blog-entry align-self-stretch flex-column-reverse" style="width: -webkit-fill-available;">
                                <a href="#" data-toggle="modal" data-target="#open" data-whatever="{{$event->id}}" class="block-20" style="background-image: url('assets/files/gallery/img/{{$event->id}}/{{$event->photos[0]->file_name}}');">
                                </a>
                                <div class="text  p-2 pt-4 d-block mb-0 text-center" style="height: fit-content">
                                    <h3 class="heading"><a href="#">{{$event->title}}</a></h3>
                                </div>
                            </div>
                        </div>
                    
                    @endforeach
                </div>
                @if($gallerys->total() > 0  && $gallerys->lastPage() > 1)
                <div class="row mt-5">
                    <div class="col text-center">
                        <div class="block-27">
                            <ul>
                                <li class="{{$gallerys->currentPage()==1?"page-item--disabled":""}}"><a href="{{route("events",["page"=>$gallerys->currentPage()-1])}}">&lt;</a></li>
                                @for($i=1;$i<=$gallerys->lastPage();$i++)
                                @if($gallerys->currentPage()>=9 && $i==2)
                                <li class="">
                                    <a class="">&#8226;&#8226;&#8226;</a>
                                </li>
                                @endif
                                @if($gallerys->currentPage()>=9 && $i>=$gallerys->currentPage() && $i<$gallerys->currentPage()+8)
                                <li class=" {{$gallerys->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($gallerys->currentPage()>=9 && $i==1)
                                <li class="">
                                    <a class="" href="{{route("events",["page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($gallerys->currentPage()>=9 && $i>=$gallerys->currentPage()+8 && $gallerys->lastPage() >= $gallerys->currentPage()+9 && $i == $gallerys->lastPage())
                                <li class="">
                                    <a class="">&#8226;&#8226;&#8226;</a>
                                </li>
                                <li class="{{$gallerys->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["page"=>$gallerys->lastPage()])}}">{{$gallerys->lastPage()}}</a>
                                </li>
                                @continue
                                @elseif($gallerys->currentPage()>=9 && $i>=$gallerys->currentPage()+8 && $gallerys->lastPage() == $gallerys->currentPage()+9 && $i == $gallerys->lastPage())
                                <li class="{{$gallerys->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["page"=>$gallerys->lastPage()])}}">{{$gallerys->lastPage()}}</a>
                                </li>
                                @continue
                                @elseif($gallerys->currentPage()>=9 && $gallerys->lastPage() < $gallerys->currentPage()+9 && $i>= $gallerys->lastPage()-9)
                                <li class="">
                                    <a class="" href="{{route("events",["page"=>$i])}}">{{$i}}</a>
                                </li>
                                @continue
                                @elseif($gallerys->currentPage()>=9)
                                @continue
                                @endif
                                @if($gallerys->currentPage()<9 && $i==10)
                                <li class="">
                                    <a class="">&gt;;&#8226;&#8226;</a>
                                </li>
                                <li class="{{$gallerys->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["page"=>$gallerys->lastPage()])}}">{{$gallerys->lastPage()}}</a>
                                </li>

                                @break
                                @endif
                                <li class="{{$gallerys->currentPage()==$i?"active":""}}">
                                    <a class="" href="{{route("events",["page"=>$i])}}">{{$i}}</a>
                                </li>
                                @endfor
                                <li class="{{$gallerys->currentPage()==$gallerys->lastPage()?"page-item--disabled":""}}"><a href="{{route("events",["page"=>$gallerys->currentPage()+1])}}">&gt;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
    @else
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="content-area clearfix">
                        <div class="left-area">
                            <article class="entry not_found">
                                <div class="no-result-ucab-logo"></div>
                                <h1>No se encontraron eventos</h1>
                                <p><a href="javascript:history.back()" ><span><i class="fas fa-arrow-left"></i></span> Regresar</a></p>
                            </article> <!-- .et_pb_post -->         
                        </div> <!-- #left-area -->
                            <div id="sidebar">
                        </div> <!-- end #sidebar -->
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
            $('#open').on('hidden.bs.modal', function (e) {
                var modal = $(this)
                modal.find(".modal-body > #description").remove()
                modal.find(".modal-body > #carouselExampleControls").remove()
            })
            $('#open').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var id = button.data('whatever') 
                var modal = $(this)
                console.log(id)
                $.post('{{route("gallery.carousel")}}', {
                    "_token": "{{csrf_token()}}",
                    "id": id,
                },function(result){
                    console.log(result)
                    modal.find(".modal-body").append(result)
                })
            });
        })
    </script>
@endsection
