@extends('layouts.app')

@section('content')
    <section id="news" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Noticias</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt-4">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-lg-3">
                <form id="form-filter" action="{{ route('news') }}" method="GET">
                    <input type="hidden" name="year">
                </form>
                <select id="select-year" class="form-control" placeholder="Category" required="">
                    @for ($i = \DateTime::createFromFormat("U",strtotime('this year'))->format("Y"); $i >= 2001 ; $i--)
                        <option {{$year == $i?"selected":""}} value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    @if ($news->isNotEmpty())
        <section class="ftco-section pt-4" id="single-section">
            <div class="container">
                <div class="row">
                    @foreach($news as $key => $new)
                    <!-- Modal -->
                       <div class="modal fade" id="open-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable {{strpos($new->photo, 'pdf')?'modal-lg':''}} " role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="openTitle">{{$new->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <center>
                                    @if (strpos($new->photo, "pdf"))
                                        <div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="{{ asset('assets/files/news/img/'.$new->photo) }}"></iframe></div>
                                    @else
                                        <a href="{{ asset('assets/files/news/img/'.$new->photo) }}" target="_blank" data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click</b> para ver la foto.">
                                            <img class="block-20" src="{{ asset('assets/files/news/img/'.$new->photo) }} ">
                                        </a>
                                    @endif
                                </center>
                                <hr>
                                {!!$new->description!!}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4  ftco-animate">
                            <div class="blog-entry align-self-stretch flex-column-reverse" data-toggle="modal" data-target="#open-{{$key}}" style="width: -webkit-fill-available;">
                                @if (strpos($new->photo, 'pdf'))
                                    <center>
                                        <a href="#" data-toggle="modal" data-target="#open-{{$key}}" class="block-20" style="background-image:url('/images/pdf.png');    width: 13rem;">
                                         </a>
                                    </center>
                                    
                                @else
                                    <a href="#" data-toggle="modal" data-target="#open-{{$key}}" class="block-20" style="background-image: url('assets/files/news/img/{{$new->photo}}'); ">
                                    </a>
                                @endif
                                    
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{$new->date}}</a></div>
                                        <div><a href="#">Admin</a></div>
                                    </div>
                                    <h3 class="heading mt-3"><a href="#">{{$new->title}}</a></h3>
                                    <p>{!!$new->description!!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @if($news->total() > 0  && $news->lastPage() > 1)
                    <div class="row mt-5">
                        <div class="col text-center">
                            <div class="block-27">
                                <ul>
                                    <li class="{{$news->currentPage()==1?"page-item--disabled":""}}"><a href="{{route("news",["year"=>$year,"page"=>$news->currentPage()-1])}}"><i class="fas fa-chevron-left"></i></a></li>
                                    @for($i=1;$i<=$news->lastPage();$i++)
                                        @if($news->currentPage()>=9 && $i==2)
                                            <li class="">
                                                <a class="">&#8226;&#8226;&#8226;</a>
                                            </li>
                                        @endif
                                        @if($news->currentPage()>=9 && $i>=$news->currentPage() && $i<$news->currentPage()+8)
                                            <li class=" {{$news->currentPage()==$i?"active":""}}">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                            </li>
                                            @continue
                                        @elseif($news->currentPage()>=9 && $i==1)
                                            <li class="">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                            </li>
                                            @continue
                                        @elseif($news->currentPage()>=9 && $i>=$news->currentPage()+8 && $news->lastPage() >= $news->currentPage()+9 && $i == $news->lastPage())
                                            <li class="">
                                                <a class="">&#8226;&#8226;&#8226;</a>
                                            </li>
                                            <li class="{{$news->currentPage()==$i?"active":""}}">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$news->lastPage()])}}">{{$news->lastPage()}}</a>
                                            </li>
                                            @continue
                                        @elseif($news->currentPage()>=9 && $i>=$news->currentPage()+8 && $news->lastPage() == $news->currentPage()+9 && $i == $news->lastPage())
                                            <li class="{{$news->currentPage()==$i?"active":""}}">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$news->lastPage()])}}">{{$news->lastPage()}}</a>
                                            </li>
                                            @continue
                                        @elseif($news->currentPage()>=9 && $news->lastPage() < $news->currentPage()+9 && $i>= $news->lastPage()-9)
                                            <li class="">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                            </li>
                                            @continue
                                        @elseif($news->currentPage()>=9)
                                            @continue
                                        @endif
                                        @if($news->currentPage()<9 && $i==10)
                                            <li class="">
                                                <a class="">&gt;;&#8226;&#8226;</a>
                                            </li>
                                            <li class="{{$news->currentPage()==$i?"active":""}}">
                                                <a class="" href="{{route("news",["year"=>$year,"page"=>$news->lastPage()])}}">{{$news->lastPage()}}</a>
                                            </li>

                                            @break
                                        @endif
                                        <li class="{{$news->currentPage()==$i?"active":""}}">
                                            <a class="" href="{{route("news",["year"=>$year,"page"=>$i])}}">{{$i}}</a>
                                        </li>
                                    @endfor
                                    <li class="{{$news->currentPage()==$news->lastPage()?"page-item--disabled":""}}"><a href="{{route("news",["year"=>$year,"page"=>$news->currentPage()+1])}}"><i class="fas fa-chevron-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>    
        </section>
    @else
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="content-area clearfix">
                        <div class="left-area">
                            <article class="entry not_found">
                                <div class="no-result-ucab-logo"></div>
                                <h1>No se encontraron noticias</h1>
                                <p><a href="javascript:history.back()" ><span><i class="fas fa-arrow-left"></i></span> Regresar</a></p>
                            </article> <!-- .et_pb_post -->         
                        </div> <!-- #left-area -->
                            <div id="sidebar">
                        </div> <!-- end #sidebar -->
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
            $("select[id=select-year]").change(function(){
                $('input[name=year]').val($(this).val());
                $('input[name=page]').val(1)
                document.getElementById('form-filter').submit()
            });
        });
    </script>
@endsection


