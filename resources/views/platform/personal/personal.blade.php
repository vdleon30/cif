@extends('layouts.app')

@section('content')

<section id="personal" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Personal</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($personals->isNotEmpty())
        <section class="ftco-section pt-4 ftco-animate">

            <div class="container-fluid py-3 bg-gris">
                <div class="container">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-todos-tab" data-toggle="tab" href="#nav-todos" role="tab" aria-controls="nav-todos" aria-selected="true">CIF</a>
                            @foreach ($jobs as $job)
                                @if ($job->personals->isNotEmpty())
                                    <a class="nav-item nav-link" id="nav-{{$job->name}}-tab" data-toggle="tab" href="#nav-{{$job->name}}" role="tab" aria-controls="nav-{{$job->name}}" aria-selected="false">{{$job->name}}</a>
                                @endif
                            @endforeach
                        </div>
                    </nav>
                    <div class="tab-content my-3" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-todos" role="tabpanel" aria-labelledby="nav-todos-tab">
                            <div class="row">
                                @foreach ($personals as $personal)
                                    <div class="col-3 col-md-2 contenedor">
                                        <img src="{{ $personal->photo && file_exists('assets/files/personal/img/'.$personal->photo)?asset('assets/files/personal/img/'.$personal->photo):asset('images/no-photo.jpg') }} " alt="{{$personal->first_name}}" class="imgEquipo img-fluid d-block mx-auto">
                                        <div class="overlay-personal">
                                            <p class="txtEquipo">{{$personal->first_name}} {{$personal->last_name}}  <br> <a href="{{ route('view_personal',["id" => $personal->id]) }}">Ver más</a></p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @foreach ($jobs as $job)
                            <div class="tab-pane fade" id="nav-{{$job->name}}" role="tabpanel" aria-labelledby="nav-{{$job->name}}-tab">
                                <div class="row">
                                    @foreach ($job->personals as $personal)
                                        <div class="col-3 col-md-2 contenedor">
                                            <img src="{{ $personal->photo && file_exists('assets/files/personal/img/'.$personal->photo)?asset('assets/files/personal/img/'.$personal->photo):asset('images/no-photo.jpg') }}" alt="{{$personal->first_name}}" class="imgEquipo img-fluid d-block mx-auto">
                                            <div class="overlay-personal">
                                                <p class="txtEquipo">{{$personal->first_name}} {{$personal->last_name}}  <br> <a href="{{ route('view_personal',["id" => $personal->id]) }}">Ver más</a></p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @else
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="content-area clearfix">
                        <div class="left-area">
                            <article class="entry not_found">
                                <div class="no-result-ucab-logo"></div>
                                <h1>No se encontraron personales</h1>
                                <p><a href="javascript:history.back()" ><span><i class="fas fa-arrow-left"></i></span> Regresar</a></p>
                            </article> <!-- .et_pb_post -->         
                        </div> <!-- #left-area -->
                            <div id="sidebar">
                        </div> <!-- end #sidebar -->
                    </div>
                </div>
            </div>
        </section>
    @endif
    
@endsection

@section('scripts')
    <script>
        (function($) {
            $(document).ready(function() {
                $(".contenedor").on('click', function() {
                    $(this).children(".overlay-personal").toggleClass('altura');
                });
            });
        })(jQuery)
    </script>
@endsection
