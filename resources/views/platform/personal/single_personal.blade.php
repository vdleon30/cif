@extends('layouts.app')

@section('content')

<section id="personal" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Personal</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
        			<div class="col-md-12 mb-5">
        				<div class="teacher-details d-md-flex">
        					@if ($personal->photo && file_exists('assets/files/personal/img/'.$personal->photo))
        						<div class="img ftco-animate" style="background-image: url(/assets/files/personal/img/{{$personal->photo}});"></div>
        					@else
        						<div class="img ftco-animate" style="background-image: url(/images/no-photo.jpg);"></div>
        					@endif
        					<div class="text ftco-animate">
        						<h3>{{$personal->first_name}} {{$personal->last_name}}</h3>
	        					<span class="position">{{$personal->job->name}}</span>
	        					{!! $personal->description !!}
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@if ($personal->publications->isNotEmpty())
    <section class="ftco-section testimony-section bg-light" id="publications">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Publicaciones</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                @foreach ($personal->publications as $publication)
                    <div class="col-3">
                        <div class="card">
                        <img class="card-img-top" src="{{asset("/images/pdf.png")}}">
                        <div class="card-img-top overlay custom-control-inline">
                            <a href="{{ route('publications.download',['id'=>$publication->id]) }} " class="btn btn-outline-light" ><i class="fas fa-download" data-toggle="tooltip" data-placement="top" title="Descargar"></i></a>
                            <a href="" class="btn btn-outline-light" data-toggle="modal" data-target=".bd-example-modal-lg" data-whatever="{{$publication->id}}"><i class="fas fa-eye" data-toggle="tooltip" data-placement="top" title="Vista Previa" ></i></a>
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">{{$publication->title}}</h5>
                        </div>
                      </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section> 
@endif
     

    @endsection

@section('scripts')
   
@endsection
