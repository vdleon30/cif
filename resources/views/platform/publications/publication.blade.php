@extends('layouts.app')

@section('content')
    <!--Modals-->
    <div class="modal fade bd-example-modal-lg" id="showPreview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-body">

          </div>
        </div>
      </div>
    </div>

    <section id="publications" class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center" style="padding: 1em 0;">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Publicaciones</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($publications->isNotEmpty())
        <section class="ftco-section" id="publications">
            <div class="container">
                <div class="row justify-content-center">
                    @foreach ($publications as $publication)
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <div class="card">
                            <img class="card-img-top" src="{{asset("/images/pdf.png")}}">
                            <div class="card-img-top overlay custom-control-inline text-center">
                                <a href="{{ route('publications.download',['id'=>$publication->id]) }} " class="btn btn-outline-light" ><i class="fas fa-download" data-toggle="tooltip" data-placement="top" title="Descargar"></i></a>
                                <a href="" class="btn btn-outline-light" data-toggle="modal" data-target=".bd-example-modal-lg" data-whatever="{{$publication->id}}"><i class="fas fa-eye" data-toggle="tooltip" data-placement="top" title="Vista Previa" ></i></a>
                            </div>
                            <div class="card-body">
                              <h5 class="card-title">{{$publication->title}}</h5>
                              @if ($publication->personal_id)
                                  <p  class="card-text"><small  data-toggle="tooltip" data-placement="top" title="Ver Perfil" class="text-muted"><a href="{{ route('view_personal',["id" => $publication->personal_id]) }}">{{$publication->personal->first_name.' '.$publication->personal->last_name}}</a></small></p>
                              @else
                                  <p class="card-text"><small class="text-muted">{{$publication->author?$publication->author:"Anónimo"}}</small></p>
                              @endif
                              
                            </div>
                          </div>
                        </div>
                    @endforeach
                 </div>
             </div>
        </section> 
    @else
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="content-area clearfix">
                        <div class="left-area">
                            <article class="entry not_found">
                                <div class="no-result-ucab-logo"></div>
                                <h1>No se encontraron publicaciones</h1>
                                <p><a href="javascript:history.back()" ><span><i class="fas fa-arrow-left"></i></span> Regresar</a></p>
                            </article> <!-- .et_pb_post -->         
                        </div> <!-- #left-area -->
                            <div id="sidebar">
                        </div> <!-- end #sidebar -->
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
            $('#showPreview').on('hidden.bs.modal', function (e) {
                var modal = $(this)
                modal.find(".modal-body > .embed-responsive").remove()
            })
            $('#showPreview').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var id = button.data('whatever') 
                var modal = $(this)
                $.post('{{route("publications.preview")}}', {
                    "_token": "{{csrf_token()}}",
                    "id": id,
                },function(result){
                    console.log(result)
                    modal.find(".modal-body").append('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="'+result+'"></iframe></div>')
                })
            });
        })
    </script>
@endsection
