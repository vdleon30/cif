@extends('layouts.admin')

@section('content')
<div class="imagina-breadcrumb mb-4">
    <div class="breadcrumb-wrapper">
        <a class="breadcrumb-item" href="{{route("admin.events")}}">
            Eventos
        </a>
        <a class="breadcrumb-item">
            Editar
        </a>
    </div>
    <div class="breadcrumb-actions">
        <a href="{{route("admin.events")}}" class="btn btn-danger action">Volver</a>
    </div>
</div>
<div class="container-fluid">
    <div class="main-content-wrapper {{isset($breadcrump)?"main-content-wrapper-breadcrumb":""}}" id="edit_action">
        <h1>Editar Evento</h1>
        <form action="{{route("admin.events.update")}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="admin-section">
                <div class="admin-section-desc">
                    <p>Información Básica</p>
                </div>
                <div class="container-fluid">
                <input type="hidden" name="id" value="{{$event->id}}">

                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12">
                            <label for="title">{{ __('Título') }}</label>
                            <input id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{$event->title}}" placeholder="Título" name="title" required/>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12">
                            <label for="place">{{ __('Lugar del evento') }}</label>
                            <input id="place" class="form-control {{ $errors->has('place') ? ' is-invalid' : '' }}" value="{{$event->place}}" placeholder="Lugar del evento" name="place" required/>
                            @if ($errors->has('place'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('place') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="date">{{ __('Fecha') }}</label>
                            <input id="date" type="date" class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}" value="{{$event->date}}" placeholder="Fecha" name="date" required/>
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="hour">{{ __('Hora') }}</label>
                            <input id="hour" type="time" class="form-control {{ $errors->has('hour') ? ' is-invalid' : '' }}" value="{{$event->hour}}" placeholder="Hora" name="hour" required/>
                            @if ($errors->has('hour'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('hour') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group  col-xs-12 col-sm-12">
                            <label for="description">{{ __('Descipción') }}</label>
                            <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="6" value="{{ old('description') }}"></textarea>
                        </div>
                    </div>
                    <div class="form-row justify-content-center mt-4"> 
                        <div class="form-group  col-xs-12 col-sm-8">
                            <label for="title">Foto del evento</label>
                            <ul class="photos-list-container">
                                <li class="photo-list">
                                    <div class="m-dropzone dropzone m-dropzone--success ng-isolate-scope dz-clickable" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone="">
                                        <input id="photo" name="photo" accept=".png,.jpg,.jpeg" type="file" class="custom-file-input input-photo dz-default dz-message" lang="es">
                                        @if (strpos($event->photo, "pdf"))
                                            <img id="photo-pdf" src="{{asset("images/pdf.png")}}" height="200" width="200" >

                                            <img id="photo-img" src="" height="200" width="200" hidden="">
                                        @else
                                            <img id="photo-pdf" src="{{asset("images/pdf.png")}}" height="200" width="200" hidden="">

                                            <img id="photo-img" src="{{$event->photo?asset("/assets/files/events/img/".$event->photo):asset("/images/no-photo.jpg")}}" height="200" width="200">
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="admin_actions">
                        <button class="btn btn-primary">Guardar</button>
                        <a class="btn btn-default" href="{{route("admin.events")}}">Cancelar</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">
        CKEDITOR.config.height = 200;
        CKEDITOR.config.reset = false;
        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config.js'
        });
        CKEDITOR.instances["description"].setData("{!!$event->description!!}");
        $(document).ready(function(){
            $('#photo').change(function() {
                var id = $(this).attr("id");
                console.log(id)
                readURL(document.getElementById(id));
            });
            function readURL(input) {
                if(input.files && input.files[0]) {
                    var reader=new FileReader();
                    reader.onload=function(e) {
                        $('#'+input.id+'-img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        })
    </script>
@endsection
