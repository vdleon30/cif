@extends('layouts.admin')

@section('content')

    <div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.gallery")}}">
                Galería
            </a>
            <a class="breadcrumb-item">
                Editar
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a href="{{route("admin.gallery")}}" class="btn btn-danger action">Volver</a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="main-content-wrapper main-content-wrapper-breadcrumb" id="edit_action">
            <h1>Editar Galería</h1>
            <div class="admin-section">
                <div class="admin-section-desc">
                    <p>Información Básica</p>
                </div>
                <div class="container-fluid">
                    <form id="form-update" action="{{route("admin.gallery.update")}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$gallery->id}}">
                        <div class="form-row">
                            <div class="form-group col-xs-12 col-sm-12">
                                <label for="title">{{ __('Título') }}</label>
                                <input id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ $gallery->title }}" placeholder="Título" name="title" required/>
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="description">{{ __('Descipción') }}</label>
                                <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="6" value="{{ old('description') }}"></textarea>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('admin.gallery.photos') }}" class="dropzone">
                        @csrf
                        <input type="hidden" name="id" value="{{$gallery->id}}">
                    </form>
                    <div id="inputPhotos">
                    @if ($gallery->photos->isNotEmpty())
                        <div class="con-photo mt-4">
                            <div class="row text-center text-lg-left">
                                @foreach ($gallery->photos as $photo )

                                <div class="col-lg-4 col-md-4 col-6" id="imagen-container-{{$photo->id}}">
                                    <button value="" type="button" id="{{$photo->id}}" class="btn btn-danger" data-placement="left" title="Eliminar Foto" style="position: absolute;">
                                        <i class="fa fa-trash text-right" aria-hidden="true" ></i>
                                    </button>
                                    <a href="#" id="imagen-'.$photo->id.'" class="d-block mb-4 h-100">
                                        <img class="img-fluid img-thumbnail" src="{{asset("assets/files/gallery/img/".$gallery->id."/".$photo->file_name)}}" alt="" style="width: 200px; height:250px; overflow:hidden">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    </div>
                    <div class="admin_actions">
                        <button type="submit" onclick="document.getElementById('form-update').submit()" class="btn btn-primary">Actualizar</button>
                        <a class="btn btn-default" href="{{route("admin.gallery")}}">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>
    <script src=" {{asset("pluggin/dropzone/dist/dropzone.js")}}"></script>
    <script type="text/javascript">
        CKEDITOR.config.height = 200;
        CKEDITOR.config.reset = false;
        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config.js'
        });
        CKEDITOR.instances["description"].setData("{!!$gallery->description!!}");
        Dropzone.autoDiscover = false;
        var photos = $("#inputPhotos")
        $(document).ready(function(){
            $(".btn-danger").click(function() {
                deletePhoto($(this)[0].id)
            })
            $(function() {
                var myDropzone = new Dropzone(".dropzone");
                $(".dz-message > span").text("Haz Click ó Arrastra los archivos aquí para subirlos")

                myDropzone.on("queuecomplete", function(file) {
                    $.post('{{route("admin.gallery.get.photo")}}', {
                        "_token": "{{csrf_token()}}",
                        "id": {{$gallery->id}}
                    },function(result){
                        $(".con-photo").remove()
                        photos.append(result)
                        $(".btn-danger").click(function() {
                            deletePhoto($(this)[0].id)
                        })
                    });
                });
            })

            function deletePhoto(id) {
                $("#imagen-"+id).remove()
                $("#imagen-container-"+id).remove()
                $.post('{{route("admin.gallery.delete.photo")}}', {
                    "photo_id": id,
                    "_token": "{{csrf_token()}}",
                    "id": {{$gallery->id}},
                },function(result,status){
                })
            }
        })
    </script>
@endsection

