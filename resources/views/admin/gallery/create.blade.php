@extends('layouts.admin')

@section('content')
    <div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.gallery")}}">
                Galerías
            </a>
            <a class="breadcrumb-item">
                Crear
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a href="{{route("admin.gallery")}}" class="btn btn-danger action">Volver</a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="main-content-wrapper main-content-wrapper-breadcrumb" id="edit_action">
            <h1>Crear Galería</h1>
            <form action="{{route("admin.gallery.save")}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="admin-section">
                    <div class="admin-section-desc">
                        <p>Información Básica</p>
                    </div>
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="form-group col-xs-12 col-sm-12">
                                <label for="title">{{ __('Título') }}</label>
                                <input id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" placeholder="Título" name="title" required/>
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="description">{{ __('Descipción') }}</label>
                                <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="6" value="{{ old('description') }}"></textarea>
                            </div>
                        </div>
                        <div class="admin_actions">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a class="btn btn-default" href="{{route("admin.gallery")}}">Cancelar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">
        CKEDITOR.config.height = 200;
        CKEDITOR.config.reset = false;
        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config.js'
        });
        CKEDITOR.instances["description"].setData("{!! old('description') !!}");
    </script>
@endsection

