@extends('layouts.admin')

@section('content')

<div class="imagina-breadcrumb mb-4">
    <div class="breadcrumb-wrapper">
        <a class="breadcrumb-item" href="{{route("admin.publication")}}">
            Publicaciones
        </a>
        <a class="breadcrumb-item">
            Crear
        </a>
    </div>
    <div class="breadcrumb-actions">
        <a href="{{route("admin.publication")}}" class="btn btn-danger action">Volver</a>
    </div>
</div>
<div class="container-fluid">
    <div class="main-content-wrapper main-content-wrapper-breadcrumb" id="edit_action">
        <h1>Crear Publicación</h1>
        <form action="{{route("admin.publication.save")}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="admin-section">
                <div class="admin-section-desc">
                    <p>Información Básica</p>
                </div>
                <div class="container-fluid">
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12">
                            <label for="title">{{ __('Título') }}</label>
                            <input id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" placeholder="Título" name="title" required/>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12" id="switch">
                            <h5 class="font-weight-light ">Autor</h5>
                            <div class="custom-control custom-switch custom-control-inline">
                                <input type="checkbox" class="custom-control-input" checked="" disabled="" id="personal_check">
                                <input type="hidden" name="personal_check" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="personal_check">Personal Registrado</label>
                            </div>
                            <div class="custom-control custom-switch custom-control-inline">
                                <input type="checkbox" name="foreing" class="custom-control-input" id="foreing_check">
                                <input type="hidden" name="foreing_check" class="custom-control-input">
                                <label class="custom-control-label" for="foreing_check">Personal Foraneo</label>
                            </div>
                            <div class="custom-control custom-switch custom-control-inline">
                                <input type="checkbox" name="anonimus" class="custom-control-input" id="anonimus_check">
                                <input type="hidden" name="anonimus_check" class="custom-control-input">
                                <label class="custom-control-label" for="anonimus_check">Anónimo</label>
                            </div>
                            
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12">
                            <select name="personal_id" class="custom-select mt-3" id="personal_check_input">
                                @foreach ($personals as $personal)
                                    <option value="{{$personal->id}}">{{$personal->first_name}} {{$personal->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row" id="foreing_check_input" hidden="">
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="first_name">{{ __('Nombre') }}</label>
                            <input id="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{ old('first_name') }}" placeholder="Nombre" name="first_name"/>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="last_name">{{ __('Apellido') }}</label>
                            <input id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{ old('last_name') }}" placeholder="Apellido" name="last_name"/>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row justify-content-center mt-4"> 
                        <div class="form-group  col-xs-12 col-sm-8">
                            <label for="file">Archivo de la Publicación</label>
                            <ul class="photos-list-container">
                                <li class="photo-list">
                                    <div class="m-dropzone dropzone m-dropzone--success ng-isolate-scope dz-clickable" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone="">
                                        <input id="photo" name="file" accept=".pdf" type="file" class="custom-file-input input-photo dz-default dz-message" lang="es" data-toggle="tooltip" data-placement="top" title="Solo archivos PDF" required="">
                                        <img id="photo-img" src="{{old('file')?asset("/images/pdf.png"):asset("/images/no-photo.jpg")}}" height="200" width="200" >
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="admin_actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn btn-default" href="{{route("admin.publication")}}">Cancelar</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip()
            $('input:checkbox').click(function(){
                if ($(this)[0].checked) {
                    $("#"+$(this)[0].id+"_input").prop("hidden",false)
                    $(this).prop('disabled', true)
                    $('input:checkbox').not(this).each(function(){
                        $(this).prop('disabled', false)
                        $(this).prop('checked', false)
                        $("#"+$(this)[0].id+"_input").prop("hidden",true)
                   })

                    $('#switch input:hidden').each(function(){
                        $(this).val(0)
                   })
                    $("input[name="+$(this)[0].id+"]").val(1)
                }
            });
            

            $('#photo').change(function() {
                var id = $(this).attr("id");
                console.log(id)
                readURL(document.getElementById(id));
            });
            function readURL(input) {
                if(input.files && input.files[0]) {
                    var reader=new FileReader();
                    reader.onload=function(e) {
                        $('#'+input.id+'-img').attr('src', '{{asset('images/pdf.png')}}');
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        })
    </script>
@endsection
