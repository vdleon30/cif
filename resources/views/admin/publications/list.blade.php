@extends('layouts.admin')

@section('content')
    <div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item">
                Publicaciones
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a href="{{route("admin.publication.create")}}" class="btn btn-primary action">Crear</a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header m-0 font-weight-bold text-primary">
                <i class="fas fa-fw fa-book"></i>
                <span>Publicaciones</span>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-list" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Fecha</th>
                                <th>Registrado</th>
                                <th width="25px"></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Título</th>
                                <th>Fecha</th>
                                <th>Registrado</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title">Confirmar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="body-modal">
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <form action="{{route("admin.publication.delete")}}" method="post" >
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value=""/>
                        <a id="accept-modal"><button type="submit" class="btn btn-success btn-sm">Aceptar</button></a>
                    </form>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#table-list').DataTable( {
                "ajax": '{{route("admin.publication.get")}}',
                "columnDefs": [{ "orderable": true}],
                "iDisplayLength": 50,
                "order": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Publicaciones",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Publicaciones",
                    "search":"Buscar:"
                }
            });
            $('#myModal').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget) // Button that triggered the modal
              var recipient = button.data('whatever') // Extract info from data-* attributes
              sp = recipient.split("/")
              var modal = $(this)
              if (sp[0] == "aceptar") {
                modal.find('.modal-title').text('Eliminar Publicación')
                modal.find('.modal-body').text('¿Está seguro que desea eliminar el Publicación?')
                modal.find("input[name=id]").val(sp[1])
              }
            })
        });
    </script>
@endsection 