@extends('layouts.admin')
<style type="text/css">
    header{
        z-index: 1;
        margin: 0 25%;
        top: -2.5rem;
        width: auto;
        margin-top: -2rem;
        background-color: #5fb95b !important;
        border-radius: 0.5rem;
    }
    #header{
        width: 100%;
        position: absolute;
    }
    #component:hover{
        transform: translateY(-0.25rem);
        box-shadow: 0 2.25rem 1.5rem -1.5rem rgba(33,37,41,.3),0 0 1.5rem .5rem rgba(33,37,41,.05)!important;
        cursor: pointer;
    }
    #component:hover a{
        text-decoration: unset;
    }
</style>
@section('content')
<div class="imagina-breadcrumb">
    <div class="breadcrumb-wrapper">
        <a class="breadcrumb-item" href="{{route("admin.customize")}}">
            Personalizar
        </a>
    </div>
</div>
<div class="container-fluid mt-5">
    <div class="row margin-content">
        <div class="col-xl-6 col-md-6 col-xs-6 mb-5">
            <div  id="component" class="card border-0 shadow" >
                <a href="{{ route('admin.customize.change',["type"=>"descripcion"]) }}">
                    <div id="header">
                        <header class=" text-center custom-shadow">
                            <div class="container py-1">
                                <h4 class="font-weight-light text-white">CIF</h4>
                            </div>
                        </header>
                    </div>
                    <img src="{{asset('images/customize_1.png')}}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                        <h5 class="card-title mb-0">Breve Descripción del Centro</h5>
                        <div class="card-text text-black-50">Todos los cambios se aplican instantáneamente.</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-xl-6 col-md-6 col-xs-6 mb-5">
            <div  id="component" class="card border-0 shadow" >
                <a href="{{ route('admin.customize.change',["type"=>"gestion"]) }}">
                    <div id="header">
                        <header class=" text-center custom-shadow">
                            <div class="container py-1">
                                <h4 class="font-weight-light text-white">Filosofía de gestión</h4>
                            </div>
                        </header>
                    </div>
                    <img src="{{asset('images/customize_2.png')}}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                        <h5 class="card-title mb-0">Modificar la Filosofía de gestión</h5>
                        <div class="card-text text-black-50">Todos los cambios se aplican instantáneamente.</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-xl-6 col-md-6 col-xs-6 mb-5">
            <div  id="component" class="card border-0 shadow" >
                <a href="{{ route('admin.customize.change',["type"=>"link"]) }}">
                    <div id="header">
                        <header class=" text-center custom-shadow">
                            <div class="container py-1">
                                <h4 class="font-weight-light text-white">Enlaces de Interés</h4>
                            </div>
                        </header>
                    </div>
                    <img src="{{asset('images/customize_3.png')}}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                        <h5 class="card-title mb-0">Modificar los enlaces de interés</h5>
                        <div class="card-text text-black-50">Todos los cambios se aplican instantáneamente.</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection