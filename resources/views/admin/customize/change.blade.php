@extends('layouts.admin')
<style type="text/css">
    header{
    z-index: 1;
    margin: 0 25%;
    top: -2.5rem;
    width: auto;
    margin-top: -2rem;
    background-color: #5fb95b !important;
    border-radius: 0.5rem;
    }
    #header{
        width: 100%;
        position: absolute;
    }
    #component:hover{
                transform: translateY(-0.25rem);
    box-shadow: 0 2.25rem 1.5rem -1.5rem rgba(33,37,41,.3),0 0 1.5rem .5rem rgba(33,37,41,.05)!important;
            cursor: pointer;
    }
</style>
@section('content')
    <div class="imagina-breadcrumb">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.customize")}}">
                Personalizar
            </a>
            <a class="breadcrumb-item">
                {{ ucfirst($type) }}
            </a>
        </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row margin-content">
            @if ($type == "descripcion")
                @include('admin.customize.include.description')
            @elseif ($type == "gestion")
                @include('admin.customize.include.gestion')
            @elseif ($type == "link")
                @include('admin.customize.include.link')
            @endif
        </div>
    </div>
@endsection