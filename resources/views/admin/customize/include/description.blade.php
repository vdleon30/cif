

<div class="text-center container-fluid">
	<div  class="con-photo" >
		<div class=" text-center text-lg-left">
			<form action="{{ route('admin.customize.description.save') }}" method="POST">
				@csrf
				<div class="form-row justify-content-center"> 
	                <div class="form-group  col-xs-12 col-sm-12 text-center">
	                    <h3 for="description" class="mb-3">{{ __('Breve Descipción de CIF') }}</h3>
	                    <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="8" value=""></textarea>
	                </div>
	                <button class="btn btn-success">Actualizar</button>
	            </div>
			</form>

		</div>
	</div>
</div>
<script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">

        CKEDITOR.config.height = 400;
        CKEDITOR.config.reset = true;

        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config_customize.js'
        });
        @if (isset($items) && $items->data_y)
        	CKEDITOR.instances["description"].setData("{!!$items->data_y !!}");
        @endif
    </script>