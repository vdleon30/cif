

<div class="text-center container-fluid">
	<div  class="con-photo" >
		<div class=" text-center text-lg-left">
			<form id="gestion-form" action="{{ route('admin.customize.gestion.save') }}" method="POST">
				@csrf
				<div class="form-row justify-content-center"> 
	                <div class="form-group  col-xs-12 col-sm-12 text-center">
	                    <h3 for="description" class="mb-3">{{ __('Filosofía de la gestión del CIF') }}</h3>

	                    @if ($items->isNotEmpty())
	                    	@foreach($items as $value)
	                    	<div class="box-{{$value->data_x}}">
	                    		
	                    	<div class="form-row justify-content-between" style="">
	                    		<input type="text" class="form-control mb-3 col-10" name="title[]" placeholder="Ingrese el Título" value="{{$value->data_x}}">
	                    		<button style="z-index: 1;" value="{{$value->data_x}}" data-id="{{$value->id}}" type="button" id="remove-box-{{$value->data_x}}" class="btn btn-danger mb-3"  data-placement="left" data-toggle="tooltip" title="Eliminar Componente "><i class="fa fa-trash text-right" aria-hidden="true" ></i></button>
	                    	</div>
	                    		<input type="hidden" name="id[]" value="{{$value->id}}">
	                    		<textarea id="description-{{$value->data_x}}" class="form-control" placeholder="Descipción" name="description[]" rows="6" value="">{{$value->data_y}}</textarea>
	                    		<hr>
	                    	</div>

	                    	@endforeach
	                    @else
	                	<input type="text" class="form-control mb-3" name="title[]" placeholder="Ingrese el Título">
	                    <textarea id="description" class="form-control" placeholder="Descipción" name="description[]" rows="6" value=""></textarea>
	                    @endif
	                </div>
	                <div class="col-12" id="inputTitle">
	                	
	                </div>
	                <div class="col-12 text-right">
	                	<a class="btn btn-warning" id="addTitle">Agregar Subtitulo</a>
	                	
	                </div>
	                <div>
	                <button class="btn btn-success">Actualizar</button>
	                	
	                </div>
	            </div>
			</form>

		</div>
	</div>
</div>
@section('scripts')

<script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">

        CKEDITOR.config.height = 300;
        CKEDITOR.config.reset = true;

        @if ($items->isNotEmpty())
        	@foreach($items as $value)
	        	CKEDITOR.replace(document.querySelector( "#description-{{$value->data_x}}" ), {
		            customConfig: '/pluggin/ckeditor/config_customize.js'
		        });
        	    CKEDITOR.instances["description-{{$value->data_x}}"].setData("{!! $value->data_y !!}");
        	    $("#remove-box-{{$value->data_x}}").click(function(event) {
					$(".box-"+$(this).val()).remove();
					$("#hr-"+$(this).val()).remove();
					var x = document.createElement("INPUT");
                    x.setAttribute("type", "hidden");
                    x.setAttribute("value", $(this).data('id'));
                    x.setAttribute("name", 'delete[]');
                    document.getElementById('gestion-form').appendChild(x);

				});
        	@endforeach
        @else
        	CKEDITOR.replace(document.querySelector( "#description" ), {
	            customConfig: '/pluggin/ckeditor/config_customize.js'
	        });
        @endif
        $(document).ready(function(){
			var inputTitle = $("#inputTitle")
			var i=0
        	$("#addTitle").click(function() {
				i++;
				addTitle();
			});

			function addTitle() {
				inputTitle.append('<div class=" box-'+i+'"><div class="form-row justify-content-between" style=""><input type="text" class="form-control mb-3 col-10" name="title[]" placeholder="Ingrese el Título"><button style="z-index: 1;" value="'+i+'" type="button" id="remove-box-'+i+'" class="btn btn-danger mb-3"  data-placement="left" data-toggle="tooltip" title="Eliminar Componente "><i class="fa fa-trash text-right" aria-hidden="true" ></i></button></div><textarea id="description-'+i+'" class="form-control" placeholder="Descipción" name="description[]" rows="6" value=""></textarea></div><hr id="br-'+i+'">');

				$("#remove-box-"+i).click(function(event) {
					$(".box-"+$(this).val()).remove();
					$("#br-"+$(this).val()).remove();

				});
				CKEDITOR.replace(document.querySelector( '#description-'+i ), {
		            customConfig: '/pluggin/ckeditor/config_customize.js'
		        });
			}
		});

    </script>
    @endsection