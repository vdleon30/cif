

<div class="text-center container-fluid">
	<div  class="con-photo" >
		<div class=" text-center text-lg-left">
			<form id="gestion-form" action="{{ route('admin.customize.link.save') }}" method="POST">
				@csrf
				<div class="form-row justify-content-center"> 
					<h3 for="description" class="mb-5">{{ __('Enlaces de Interés') }}</h3>
					@if ($items->isNotEmpty())
					@foreach($items as $key =>  $value)
					<div class="form-group  col-xs-12 col-sm-12 text-center">
						<div class="box-{{$value->id}}">
							<input type="hidden" name="id[]" value="{{$value->id}}">
							<div class="border col-12 py-3 rounded mb-3">
								<button style="z-index: 1;" value="{{$value->id}}" data-id="{{$value->id}}" type="button" id="remove-box-{{$value->id}}" class="btn btn-danger mb-3"  data-placement="top" data-toggle="tooltip" title="Eliminar Grupo "><i class="fa fa-trash text-right" aria-hidden="true" ></i></button>

								<div class="form-group  col-xs-12 col-sm-12 text-center">
									<h2>{{$value->data_x}}</h2>
									<input type="hidden" class="form-control" name="title_group[{{$key}}]"  value="{{$value->data_x}}">

								</div>
								@foreach ($value->links as $element)
								<div class="col-12 linkk-{{$element->id}}">
									<input type="hidden" class="form-control" name="id[{{$key}}][]"  value="{{$element->id}}">
									<div class="form-row  col-xs-12 col-sm-12 text-center">
										<div class="form-group  col-5 text-left">
											<input type="text" class="form-control" name="title_link[{{$key}}][]" placeholder="Titulo" value="{{$element->data_x}}">
										</div>
										<div class="form-group  col-6 ">
											<input type="url" class="form-control" name="link[{{$key}}][]" placeholder="Link" value="{{$element->data_y}}">
										</div>
										<button style="z-index: 1;" value="{{$element->id}}" data-id="{{$value->id}}" type="button" id="remove-linkk-{{$element->id}}" class="btn btn-danger mb-3"  data-placement="top" data-toggle="tooltip" title="Eliminar Link "><i class="fa fa-trash text-right" aria-hidden="true" ></i></button>
									</div>
								</div>

								@endforeach
								<div class="col-12 " id="inputLink{{$value->id}}">
								</div>
								<div class="col-12 text-right">
									<a class="btn btn-info" onclick="addLink('{{$value->id}}','{{$key}}')">Nuevo Link</a>
								</div>
								
							</div>
							<hr>

						</div>
					</div>

					@endforeach
					<div class="col-12" id="inputTitle">
					</div>
					<div class="col-12 text-right">
						<a class="btn btn-warning" id="addTitle">Nuevo Grupo</a>
					</div>
					@else
						<div class="border col-12 py-3 rounded mb-3">
							<div class="form-group  col-xs-12 col-sm-12 text-center">
								<input type="text" class="form-control mb-3" name="title_group[]" placeholder="Ingrese el Título del grupo" required="">
							</div>
							<div class="form-row  col-xs-12 col-sm-12 text-center">
								<div class="form-group  col-6 ">
									<input type="text" class="form-control" name="title_link[0][]" placeholder="Titulo">
								</div>
								<div class="form-group  col-6 ">
									<input type="url" class="form-control" name="link[0][]" placeholder="Link">
								</div>
							</div>
						</div>
					@endif
					
					<div>
						<button class="btn btn-success">Actualizar</button>

					</div>
				</div>
			</form>

		</div>
	</div>
</div>
@section('scripts')


<script type="text/javascript">

	@if ($items->isNotEmpty())
	@foreach($items as $value)
	
	$("#remove-box-{{$value->id}}").click(function(event) {
		$(".box-"+$(this).val()).remove();
		$("#hr-"+$(this).val()).remove();
		var x = document.createElement("INPUT");
		x.setAttribute("type", "hidden");
		x.setAttribute("value", $(this).data('id'));
		x.setAttribute("name", 'delete[]');
		document.getElementById('gestion-form').appendChild(x);

	});
	@if ($value->links->isNotEmpty())
		@foreach ($value->links as $element)
			$("#remove-linkk-{{$element->id}}").click(function(event) {
				$(".linkk-"+$(this).val()).remove();
				var x = document.createElement("INPUT");
				x.setAttribute("type", "hidden");
				x.setAttribute("value", '{{$element->id}}');
				x.setAttribute("name", 'delete[]');
				document.getElementById('gestion-form').appendChild(x);

			});
		@endforeach

	@endif
	@endforeach
	
	@endif
		var j=0

	function addLink(argument,key) {
			j++
			$("#inputLink"+argument).append('<div class="link-'+j+'"><div class="form-row  text-center"><div class="form-group  col-6 "><input type="text" class="form-control" name="title_link['+key+'][]" placeholder="Titulo"></div><div class="form-group  col-6 "><input type="url" class="form-control" name="link['+key+'][]" placeholder="Link"></div></div></div>');

			$("#remove-link-"+j).click(function(event) {
				$(".link-"+$(this).val()).remove();
				$("#br_link-"+$(this).val()).remove();

			});
		}
	$(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 

		var inputTitle = $("#inputTitle")
		var i=0
		$("#addTitle").click(function() {
			i++;
			addTitle();
		});

		function addTitle() {
			inputTitle.append('<div class=" box-'+i+'"><div class="border col-12 py-3 rounded mb-3"><div class="form-group  col-xs-12 col-sm-12 text-center"><input type="text" class="form-control mb-3" name="title_group[new'+i+']" placeholder="Ingrese el Título del grupo" required=""></div><div class="form-row  col-xs-12 col-sm-12 text-center"><div class="form-group  col-6 "><input type="text" class="form-control" name="title_link[new'+i+'][]" placeholder="Titulo"></div><div class="form-group  col-6 "><input type="url" class="form-control" name="link[new'+i+'][]" placeholder="Link"></div></div></div><hr id="br-'+i+'">');
            $('[data-toggle="tooltip"]').tooltip(); 

			$("#remove-box-"+i).click(function(event) {
				$(".box-"+$(this).val()).remove();
				$("#br-"+$(this).val()).remove();

			});
			
		}

		
	});
</script>
@endsection