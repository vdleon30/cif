@extends('layouts.admin')

@section('content')
<div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.users")}}">
                Usuarios
            </a>
            <a class="breadcrumb-item">
                Editar
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a href="{{route("admin.users")}}" class="btn btn-danger action">Volver</a>
        </div>
    </div>

<div class="container-fluid">
     <div class="main-content-wrapper {{isset($breadcrump)?"main-content-wrapper-breadcrumb":""}}" id="edit_action">

    <h1>Editar Usuario</h1>

    <form action="{{route("admin.users.update")}}" method="post">
        {{ csrf_field() }}

        <div class="admin-section">
            <div class="admin-section-desc">
                <p>Información Básica</p>
            </div>

            <div class="container-fluid">
                <h5 class="font-weight-light ">Datos Personales</h5>
                <input type="hidden" name="id" value="{{$edit_user->id}}">
                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="first_name">{{ __('Nombre') }}</label>
                        <input id="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{$edit_user->first_name}}" placeholder="Nombre" name="first_name" required/>
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="last_name">{{ __('Apellido') }}</label>
                        <input id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{$edit_user->last_name}}" placeholder="Apellido" name="last_name"  required/>
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="dni">{{ __('Cédula') }}</label>
                        <input id="dni" type="dni" class="form-control {{ $errors->has('dni') ? ' is-invalid' : '' }}" value="{{$edit_user->dni}}" placeholder="Cédula" name="dni" required/>
                        @if ($errors->has('dni'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('dni') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="mobile_number">{{ __('Número de Telefono') }}</label>
                        <input id="mobile_number" class="form-control {{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" value="{{$edit_user->mobile_number}}" placeholder="Número de Telefono" name="mobile_number" required/>
                        @if ($errors->has('mobile_number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="email">{{ __('Correo Eléctronico') }}</label>
                        <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$edit_user->email}}" placeholder="Correo Eléctronico" name="email" required/>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="username">{{ __('Usuario') }}</label>
                        <input id="username" type="username" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{$edit_user->username}}" placeholder="Usuario" name="username" required/>
                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <h5 class="font-weight-light ">Cambiar Contraseña</h5>

                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="password">{{ __('Contraseña') }}</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" >
                        
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="password-confirm">{{ __('Confirmar Contraseña') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" >
                    </div>
                </div>
                @role("admin")
                <div class="form-row" hidden="" >
                    <div class="col-xs-12 col-sm-6">
                        <label>Rol</label>
                        <select class="form-control" name="rol" id="roles" onchange="" required>
                           <option value="assistant">Asistente</option>
                        </select>
                    </div>
                </div>
                @endrole
                

                <div id="inputRoles"></div>
            </div>
        </div>

        <div class="admin_actions">
            <button class="btn btn-primary">Guardar</button>
            <a class="btn btn-default" href="{{route("admin.users")}}">Cancelar</a>
        </div>
    </form>
</div>
</div>

@endsection

@section('scripts')
    <script type="text/javascript">
        

    </script>
@endsection
