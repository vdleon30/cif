@extends('layouts.admin')

@section('content')
<div class="imagina-breadcrumb mb-4">
    <div class="breadcrumb-wrapper">
        <a class="breadcrumb-item" href="{{route("admin.news")}}">
            Noticias
        </a>
        <a class="breadcrumb-item">
            Editar
        </a>
    </div>
    <div class="breadcrumb-actions">
        <a href="{{route("admin.news")}}" class="btn btn-danger action">Volver</a>
    </div>
</div>
<div class="container-fluid">
    <div class="main-content-wrapper {{isset($breadcrump)?"main-content-wrapper-breadcrumb":""}}" id="edit_action">
        <h1>Editar Noticia</h1>
        <form action="{{route("admin.news.update")}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="admin-section">
                <div class="admin-section-desc">
                    <p>Información Básica</p>
                </div>
                <div class="container-fluid">
                <input type="hidden" name="id" value="{{$new->id}}">

                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-12">
                            <label for="title">{{ __('Título') }}</label>
                            <input id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{$new->title}}" placeholder="Título" name="title" required/>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12 col-sm-6">
                            <label for="date">{{ __('Fecha') }}</label>
                            <input id="date" type="date" class="form-control {{ $errors->has('date') ? ' is-invalid' : '' }}" value="{{$new->date}}" placeholder="Fecha" name="date"  required/>
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group  col-xs-12 col-sm-12">
                            <label for="description">{{ __('Descipción') }}</label>
                            <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="6" value="{{ old('description') }}"></textarea>
                        </div>
                    </div>
                    <div class="form-row justify-content-center mt-4"> 
                        <div class="form-group  col-xs-12 col-sm-8">
                            <label for="title">Foto de la noticia</label>
                            <ul class="photos-list-container">
                                <li class="photo-list">
                                    <div class="m-dropzone dropzone m-dropzone--success ng-isolate-scope dz-clickable" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone="">
                                        <input id="photo" name="photo" accept=".png,.jpg,.jpeg,.pdf" type="file" class="custom-file-input input-photo dz-default dz-message" lang="es">
                                        @if (strpos($new->photo, "pdf"))
                                            <img id="photo-pdf" src="{{asset("images/pdf.png")}}" height="200" width="200" >

                                            <img id="photo-img" src="" height="200" width="200" hidden="">
                                        @else
                                            <img id="photo-pdf" src="{{asset("images/pdf.png")}}" height="200" width="200" hidden="">

                                            <img id="photo-img" src="{{$new->photo?asset("/assets/files/news/img/".$new->photo):asset("/images/no-photo.jpg")}}" height="200" width="200">
                                        @endif


                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="admin_actions">
                        <button class="btn btn-primary">Guardar</button>
                        <a class="btn btn-default" href="{{route("admin.news")}}">Cancelar</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">
        CKEDITOR.config.height = 200;
        CKEDITOR.config.reset = false;
        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config.js'
        });
        CKEDITOR.instances["description"].setData("{!!$new->description!!}");
        $(document).ready(function(){
            $('#photo').change(function() {
                var id = $(this).attr("id");
                console.log(id)
                readURL(document.getElementById(id));
            });
            function readURL(input) {
                if(input.files && input.files[0]) {
                    var reader=new FileReader();
                    reader.onload=function(e) {
                        var s = e.target.result.indexOf("application/pdf");
                        if (s > 0) {
                            $('#'+input.id+'-pdf').removeAttr('hidden');
                            $('#'+input.id+'-img').attr('hidden','');
                        }else{
                            $('#'+input.id+'-img').attr('src', e.target.result);
                            $('#'+input.id+'-pdf').attr('hidden','');
                            $('#'+input.id+'-img').removeAttr('hidden');
                        }
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        })
    </script>
@endsection
