@extends('layouts.admin')

@section('content')
<div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.personal")}}">
                Personal
            </a>
            <a class="breadcrumb-item">
                Editar
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a href="{{route("admin.personal")}}" class="btn btn-danger action">Volver</a>
        </div>
    </div>
<div class="container-fluid">

<div class="main-content-wrapper {{isset($breadcrump)?"main-content-wrapper-breadcrumb":""}}" id="edit_action">

    <h1>Editar Personal</h1>

    <form action="{{route("admin.personal.update")}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $personal->id }}">
        <div class="admin-section">
            <div class="admin-section-desc">
                <p>Información Básica</p>
            </div>

            <div class="container-fluid">
                <h5 class="font-weight-light ">Datos Personales</h5>

                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="first_name">{{ __('Nombre') }}</label>
                        <input id="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{ $personal->first_name }}" placeholder="Nombre" name="first_name" required/>
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="last_name">{{ __('Apellido') }}</label>
                        <input id="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{ $personal->last_name }}" placeholder="Apellido" name="last_name" required/>
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="email">{{ __('Correo Eléctronico') }}</label>
                        <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $personal->email }}" placeholder="Correo Eléctronico" name="email" required/>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="mobile_number">{{ __('Número de Telefono') }}</label>
                        <input id="mobile_number" class="form-control {{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" value="{{$personal->mobile_number }}" placeholder="Número de Telefono" name="mobile_number" required/>
                        @if ($errors->has('mobile_number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="job">{{ __('Cargo') }}</label>
                        <select class="form-control" name="job">
                            @foreach ($jobs as $job)
                                <option value="{{$job->id}}" {{$personal->job_id==$job->id?"selected":""}}>{{$job->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group  col-xs-12 col-sm-12">
                        <label for="description">{{ __('Descipción') }}</label>
                        <textarea id="description" class="form-control" placeholder="Descipción" name="description" rows="6" value="{{ old('description') }}"></textarea>
                    </div>
                </div>
                <div class="form-row justify-content-center mt-4"> 
                    <div class="form-group  col-xs-12 col-sm-8">
                        <label for="title">Foto de perfil</label>
                        <ul class="photos-list-container">
                            <li class="photo-list">
                                <div class="m-dropzone dropzone m-dropzone--success ng-isolate-scope dz-clickable" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone="">
                                    <input id="photo" name="photo" accept=".png,.jpg,.jpeg" type="file" class="custom-file-input input-photo dz-default dz-message" lang="es">
                                    <img id="photo-img" src="{{$personal->photo?asset("assets/files/personal/img/".$personal->photo):asset("/images/no-photo.jpg")}}" height="200" width="200">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="admin_actions">
            <button class="btn btn-primary">Actualizar</button>
            <a class="btn btn-default" href="{{route("admin.personal")}}">Cancelar</a>
        </div>
    </form>
</div>
</div>

@endsection

@section('scripts')
    <script src=" {{asset("pluggin/ckeditor/ckeditor.js")}}"></script>

    <script type="text/javascript">
        CKEDITOR.config.height = 200;
        CKEDITOR.config.reset = false;
        CKEDITOR.replace(document.querySelector( '#description' ), {
            customConfig: '/pluggin/ckeditor/config.js'
        });
        CKEDITOR.instances["description"].setData("{!!$personal->description!!}");
        $(document).ready(function(){
            $('#photo').change(function() {
                var id = $(this).attr("id");
                console.log(id)
                readURL(document.getElementById(id));
            });
            function readURL(input) {
                if(input.files && input.files[0]) {
                    var reader=new FileReader();
                    reader.onload=function(e) {
                        $('#'+input.id+'-img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        })
    </script>
@endsection
