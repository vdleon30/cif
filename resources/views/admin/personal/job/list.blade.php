@extends('layouts.admin')

@section('content')
    <div class="imagina-breadcrumb mb-4">
        <div class="breadcrumb-wrapper">
            <a class="breadcrumb-item" href="{{route("admin.personal")}}">
                Personal
            </a>
            <a class="breadcrumb-item">
                Cargo
            </a>
        </div>
        <div class="breadcrumb-actions">
            <a data-toggle="modal" href="#" data-target="#createModal" data-whatever="create/" class="btn btn-primary action">Crear</a>
        </div>
    </div>
    <div class="container-fluid">
   
    <div class="card mt-3">
        <div class="card-header m-0 font-weight-bold text-primary">
            <i class="fas fa-fw fa-user-tie"></i>
            <span>Cargo</span>
        </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-list" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Registrado</th>
                                <th width="25px"></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Registrado</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route("admin.personal.job.save")}}" method="post" >
                    {{ csrf_field() }}
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" id="body-modal">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="name">{{ __('Nombre') }}</label>
                                <input id="name" class="form-control " value="" placeholder="Nombre" name="name" required/>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a id="accept-modal"><button type="submit" class="btn btn-success btn-sm">Crear</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route("admin.personal.job.update")}}" method="post" >
                    {{ csrf_field() }}
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" id="body-modal">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="name">{{ __('Nombre') }}</label>
                                <input id="name" class="form-control " value="" placeholder="Nombre" name="name" required/>
                                <input type="hidden" name="id" value=""/>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="hidden" name="id" value=""/>

                            <a id="accept-modal"><button type="submit" class="btn btn-success btn-sm">Actualizar</button></a>

                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" id="body-modal">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <form action="{{route("admin.personal.job.delete")}}" method="post" >
                     {{ csrf_field() }}
                    <input type="hidden" name="id" value=""/>

                        <a id="accept-modal"><button type="submit" class="btn btn-success btn-sm">Aceptar</button></a>

                     </form>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#table-list').DataTable( {
                "ajax": '{{route("admin.personal.job.get")}}',
                "columnDefs": [{ "orderable": true}],
                "iDisplayLength": 50,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Cargo",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Cargo",
                    "search":"Buscar:"
                }
            });
            $('#myModal').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget) // Button that triggered the modal
              var recipient = button.data('whatever') // Extract info from data-* attributes
              sp = recipient.split("/")
              var modal = $(this)
              if (sp[0] == "aceptar") {
                modal.find('.modal-title').text('Eliminar Cargo')
                modal.find('.modal-body').text('¿Está seguro que desea eliminar el Cargo?')
                modal.find("input[name=id]").val(sp[1])
              }
            })

            $('#createModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                sp = recipient.split("/")
                var modal = $(this)
                if (sp[0] == "create") {
                modal.find('.modal-title').text('Crear Cargo')
                modal.find("input[name=id]").val(sp[1])
                }
            })

            $('#editModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                sp = recipient.split("/")
                var modal = $(this)
                if (sp[0] == "edit") {
                    modal.find('.modal-title').text('Editar Cargo')
                    modal.find("input[name=id]").val(sp[1])
                    modal.find("input[name=name]").val(button.parent().parent().parent().children()[0].innerText)
                }
            })
        });
    </script>
@endsection 