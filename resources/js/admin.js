global.$ = window.jQuery = require('jquery/dist/jquery');


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./admin/jquery/jquery.min');
require('moment/moment');
require('./admin/bootstrap/js/bootstrap.bundle.min');
require('./admin/jquery-easing/jquery.easing.min');
require('./admin/chart.js/Chart');
require('./admin/datatables/jquery.dataTables');
require('datatables.net-bs4/js/dataTables.bootstrap4');
require('./admin/sb-admin-2');
require('./notify/notify');
