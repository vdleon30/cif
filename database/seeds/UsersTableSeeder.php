<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['users' => [

            ['first_name'    => 'Administrador',
             'last_name'     => 'Administrador',
             'username'      => 'admin',
             'mobile_number' => '1234567',
             'dni'           => '0000000',
             'email'         => 'admin@admin.com',
             'password'      => Hash::make("admin1234"),
             'created_at'    => gmdate("Y-m-d H:i:s"),
             'updated_at'    => gmdate("Y-m-d H:i:s")],
        ]];

        // Insertar datos en la BD
    
        foreach($data['users'] as $users) 
        {
            // Inserta los categorias en la BD

            DB::table('users')->insert([
                'first_name'    => $users['first_name'], 
                'last_name'     => $users['last_name'],
                'username'      => $users['username'], 
                'mobile_number' => $users['mobile_number'], 
                'dni'           => $users['dni'], 
                'email'         => $users['email'],
                'password'      => $users['password'],
                'created_at'    => $users['created_at'],
                'updated_at'    => $users['updated_at'], 
            ]);

            //Usuario Administrador
            $user = User::where('first_name','Administrador')->first();
            $admin = Role::where('name','admin')->first();
            
            $user->roles()->attach($admin->id);
            $user->save();
        }
    }
}
