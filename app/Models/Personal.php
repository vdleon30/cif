<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
	protected $table = 'personal';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile_number','photo','description','job_id'
    ];

    public function job()
    {
    	return $this->belongsTo('App\Models\PersonalJob');
    }

    public function publications()
    {
    	return $this->hasMany('App\Models\Publication');
    }
}
