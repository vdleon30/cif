<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalJob extends Model
{
    protected $table = 'personal_job';

	protected $fillable = [
        'name'
    ];

    public function personals()
    {
        return $this->hasMany('App\Models\Personal','job_id');
    }
}
