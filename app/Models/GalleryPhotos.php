<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryPhotos extends Model
{
     protected $table = 'gallery_photos';

	protected $fillable = [
        'order', 'gallery_id','file_name'
    ];

    public function gallery()
    {
    	return $this->belongsTo('App\Models\Gallery','id','gallery_id');
    }
}
