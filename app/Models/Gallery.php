<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    protected $fillable = [
        'title', 'description'
    ];

    public function photos()
    {
        return $this->hasMany('App\Models\GalleryPhotos','gallery_id','id');
    	
    }
}
