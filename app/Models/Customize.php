<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customize extends Model
{
    protected $table = 'customize';

    protected $fillable = [
        'type',
        'data_x',
        'data_y',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
