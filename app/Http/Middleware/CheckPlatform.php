<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\View;
use App\Models\Platform;
use Closure;
use App\User;
class CheckPlatform
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('platform', Platform::currentPlatform());
        if (User::getCurrent()) {
            return redirect()->route("admin.dashboard");
            
        }
        return $next($request);
    }
}
