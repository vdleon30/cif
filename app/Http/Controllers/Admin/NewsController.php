<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\News;

class NewsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "news");
    }

    public function index(){
        $user = User::getCurrent();
        

        return view("admin.news.list",["breadcrumb"=>true]);
    }

    public function getList(){
        $news = News::get(); 
        $user = User::getCurrent();
        
        $news_list=[];
       
        foreach($news as $new){
            $hour = \DateTime::createFromFormat("H:i",$new->hour);
            if ($user->hasRole('admin')) {
               $news_list[] = [$new->title,$new->date,$new->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.news.edit",["id"=>$new->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$new->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$new->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
                ];
            }else{
               $news_list[] = [$new->title,$new->date,$new->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.news.edit",["id"=>$new->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>'
                ];
            }
        		
        }
        return response()->json(['data' => $news_list]);
    }

    public function getCreate()
    {
        return view("admin.news.create",["breadcrumb"=>true]);
    }

    public function create(Request $request)
    {
    	$data = $request->only(["title","description","date","hour","place"]);

    	$validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'date' => ['required','string'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.news.create")->withErrors($validator)->withInput();
        }
        $new = News::create([
        	'title' => ucfirst($data['title']),
            'description' => $data['description'],
            'date' => $data['date'],
        ]);
        $type = $_FILES['photo']['type'];

        if (isset($request->photo)) {
            if (strpos($type, 'pdf')) {
                $file_name = "CIF_".$new->id."_".strtotime('now').".pdf";
            }else{
                $file_name = "CIF_".$new->id."_".strtotime('now').".png";

            }
            $request->photo->storeAs('assets/files/news/img',$file_name,'uploads');
            News::find($new->id)->update(["photo" => $file_name]);
        }

        return redirect()->route("admin.news")->with(["message_success"=>"Noticia creada exitosamente."]);
    }

    public function getEdit($id){
        $user = User::getCurrent();
        $new = News::find($id);
        return view("admin.news.edit",["user"=>$user,"new"=>$new,"breadcrumb"=>true]);
    }

    public function update(Request $request)
    {
        $data = $request->only(["id","title","description","date","hour","place"]);
        $new = News::find($data["id"]);
        if (!$new) {
            return redirect()->route("admin.news")->with(["message_error"=>"Noticia no disponible."]);
        }
        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'date' => ['required','string'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.news.edit",["id"=>$data["id"]])->withErrors($validator)->withInput();
        }

        News::find($data["id"])->update([
            'title' => ucfirst($data['title']),
            'description' => $data['description'],
            'date' => $data['date'],
        ]);

        $new = News::find($data["id"]);

        if (isset($request->photo)) {
            if ($new->photo) {
                Storage::disk('uploads')->delete('/assets/files/news/img/'.$new->photo);
            }
            $type = $_FILES['photo']['type'];
            if (strpos($type, 'pdf')) {
                $file_name = "CIF_".$new->id."_".strtotime('now').".pdf";
            }else{
                $file_name = "CIF_".$new->id."_".strtotime('now').".png";

            }
            $request->photo->storeAs('assets/files/news/img',$file_name,'uploads');
            news::find($new->id)->update(["photo" => $file_name]);
        }

        return redirect()->route("admin.news.edit",["id"=>$data["id"]])->with(["message_success"=>"Noticia actualizada exitosamente."]);
    }

    public function delete(Request $request)
    {
        $new_id=$request->get("id");
        $current_user=User::getCurrent();

        $new = News::where("id",$new_id);
        News::where("id",$new_id)->delete();

        if (isset($new->photo) && file_exists('assets/files/news/img/'.$new->photo)) {
            Storage::disk('uploads')->delete('assets/files/news/img/'.$new->photo);
        }

        return redirect()->route("admin.news")->with(["message_success"=>"Noticia eliminada exitosamente"]);
    }
}
