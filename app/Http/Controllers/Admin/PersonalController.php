<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Models\Personal;
use App\Models\PersonalJob;
use App\Models\Role;

class PersonalController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "personal");
    }

    public function index(){
        $user = User::getCurrent();

        return view("admin.personal.list",["breadcrumb"=>true,"sub_menu_active"=>"personal_list"]);
    }

    public function getList(){
        $personals = Personal::get(); 
        $user = User::getCurrent();

        $personals_list=[];
       
        foreach($personals as $personal){
    		
            if ($user->hasRole('admin')) {
               $personals_list[] = [$personal->email."<span>".$personal->first_name." ".$personal->last_name."</span>",$personal->job->name,$personal->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.personal.edit",["id"=>$personal->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$personal->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$personal->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
                ];
            }else{
               $personals_list[] = [$personal->email."<span>".$personal->first_name." ".$personal->last_name."</span>",$personal->job->name,$personal->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.personal.edit",["id"=>$personal->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>'
                ];
            }
        }
        return response()->json(['data' => $personals_list]);
    }
    public function getCreate()
    {
        $user = User::getCurrent();
        $jobs = PersonalJob::orderBy("name","ASC")->get();

        if ($jobs->isEmpty()) {
            return redirect()->route('admin.personal.job')->with(["message_error"=>"Primero debe crear un Cargo."]);
        }

        return view("admin.personal.create",["breadcrumb"=>true,"jobs"=>$jobs]);
    }

    public function create(Request $request)
    {
        $data=$request->only(["first_name","last_name","mobile_number","email","job","description","photo"]);

        $current_user = User::getCurrent();
        
       
        $validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.personal.create")->withErrors($validator)->withInput();
        }

        $personal = Personal::create([
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'description'   => $data['description'],
            'job_id'   => $data['job'],
        ]);
        if (isset($request->photo)) {
            $file_name = "CIF_".$personal->id."_".strtotime('now').".png";
            $request->photo->storeAs('assets/files/personal/img',$file_name,'uploads');
            Personal::find($personal->id)->update(["photo"=>$file_name]);
        }


        return redirect()->route("admin.personal")->with(["message_success"=>"Personal creado exitosamente."]);
    }

    public function getEdit($id){
        $user = User::getCurrent();
        $personal = Personal::find($id);
        $jobs = PersonalJob::orderBy("name","ASC")->get();
        
        return view("admin.personal.edit",["user"=>$user,"personal"=>$personal,"jobs"=>$jobs,"breadcrumb"=>true]);
    }

    public function update(Request $request)
    {
        $data=$request->only(["id","first_name","last_name","mobile_number","email","job","description","photo"]);
        $edit_personal = Personal::find($data["id"]);

        $validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.personal.edit")->withErrors($validator)->withInput();
        }

        Personal::find($data["id"])->update([
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'description'   => $data['description'],
            'job_id'   => $data['job'],
        ]);
        if (isset($request->photo)) {
            Storage::disk('uploads')->delete('/assets/files/provider/img/'.$edit_personal->photo);
            $file_name = "CIF_".$personal->id."_".strtotime('now').".png";
            $request->photo->storeAs('assets/files/personal/img',$file_name,'uploads');
            Personal::find($personal->id)->update(["photo"=>$file_name]);
        }

        return redirect()->route("admin.personal")->with(["message_success"=>"Personal actualizado exitosamente."]);
    }
    public function delete(Request $request){
        $personal_id=$request->get("id");
        $current_user=User::getCurrent();
        $personal = Personal::where("id",$personal_id)->delete();

        return redirect()->route("admin.personal")->with(["message_success"=>"Personal eliminado exitosamente"]);
    }

    public function indexJob(){
        $user = User::getCurrent();

        return view("admin.personal.job.list",["breadcrumb"=>true,"sub_menu_active"=>"job_list"]);
    }

    public function getListJob(){
        $jobs = PersonalJob::get(); 
        $user = User::getCurrent();
        
        $jobs_list=[];
       
        foreach($jobs as $job){
            if ($user->hasRole('admin')) {
               $jobs_list[] = [$job->name,$job->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="#" data-toggle="modal" data-target="#editModal" data-whatever="edit/'.$job->id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$job->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$job->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
                ];
            }else{
               $jobs_list[] = [$job->name,$job->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="#" data-toggle="modal" data-target="#editModal" data-whatever="edit/'.$job->id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>'
                ];
            }
            
        }
        return response()->json(['data' => $jobs_list]);
    }

    public function createJob(Request $request)
    {

        $data=$request->only(["name"]);
        $current_user = User::getCurrent();
        
       
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'unique:personal_job'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.personal.job")->withErrors($validator)->withInput();
        }

        $job = PersonalJob::create([
            'name' => ucfirst($data['name']),
        ]);

        return redirect()->route("admin.personal.job")->with(["message_success"=>"Oficio creado exitosamente."]);
    }

    public function updateJob(Request $request)
    {

        $data=$request->only(["name","id"]);
        $current_user = User::getCurrent();
        
        $job = PersonalJob::find($data['id']);

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'unique:personal_job,name,'.$job->id],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.personal.job")->withErrors($validator)->withInput();
        }

        PersonalJob::find($data['id'])->update([
            'name' => ucfirst($data['name']),
        ]);

        return redirect()->route("admin.personal.job")->with(["message_success"=>"Oficio actualizado exitosamente."]);
    }

    public function deleteJob(Request $request){
        $job_id=$request->get("id");
        $current_user=User::getCurrent();
        $job = PersonalJob::where("id",$job_id)->delete();

        return redirect()->route("admin.personal.job")->with(["message_success"=>"Oficio eliminado exitosamente"]);
    }
}
