<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Personal;
use App\Models\Publication;

class PublicationController extends Controller
{
    
    public function __construct()
    {
        View::share('menu_active', "publication");
    }

    public function index(){
        $user = User::getCurrent();
        

        return view("admin.publications.list",["breadcrumb"=>true]);
    }

    public function getList(){
        $publications = Publication::get(); 
        $user = User::getCurrent();
        
        $publications_list=[];
       
        foreach($publications as $publication){
        	$author = null;
        	if ($publication->personal_id) {
        		$author = $publication->personal->first_name.' '.$publication->personal->last_name;
        	}else if ($publication->author) {
        		$author = $publication->author;
        	}else
        		$author = "Anónimo";

            if ($user->hasRole('admin')) {
               $publications_list[] = [$publication->title,$author,$publication->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.publication.edit",["id"=>$publication->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$publication->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$publication->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
                ];
            }else{
               $publications_list[] = [$publication->title,$author,$publication->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.publication.edit",["id"=>$publication->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>'
                ];
            }
    		
        }
        return response()->json(['data' => $publications_list]);
    }

    public function getCreate()
    {
        $personals = Personal::orderBy("first_name","ASC")->get(); 

        return view("admin.publications.create",["breadcrumb"=>true,"personals"=>$personals]);
    }

    public function create(Request $request)
    {
    	$data = $request->only(['title', 'personal_id', 'author', 'file','personal_check','foreing_check','anonimus_check','first_name','last_name']);
    	$validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'file' => ['required'],
        ]);
 		
        if ($validator->fails()) {
            return redirect()->route("admin.publication.create")->withErrors($validator)->withInput();
        }
        if (!$data['personal_check'] || $data['personal_check'] == 0)
        	$data['personal_id'] = null;
        
        if ($data['foreing_check'] && $data['foreing_check'] == 1) {
        	$validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
	        ]);
	 		
	        if ($validator->fails()) {
	            return redirect()->route("admin.publication.create")->withErrors($validator)->withInput();
	        }
        	$author = ucfirst($data['first_name']).' '.ucfirst($data['last_name']);
        }
        else
        	$author = null;

        $publication = Publication::create([
        	'title' => $data['title'],
            'personal_id' => $data['personal_id'],
            'author' => $author,
        ]);

        if (isset($request->file)) {
            $file_name = "CIF_".$publication->id."_".strtotime('now').".pdf";
            $request->file->storeAs('assets/files/publications',$file_name,'uploads');
            Publication::find($publication->id)->update(["file" => $file_name]);
        }

        return redirect()->route("admin.publication")->with(["message_success"=>"Publicación creada exitosamente."]);
    }

    public function getEdit($id){
        $publication = Publication::find($id);
        $personals = Personal::orderBy("first_name","ASC")->get(); 

        return view("admin.publications.edit",["personals"=>$personals,"publication"=>$publication,"breadcrumb"=>true]);
    }

    public function update(Request $request)
    {
        $data = $request->only(['id','title', 'personal_id', 'author', 'file','personal_check','foreing_check','anonimus_check','first_name','last_name']);

    	$validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
        ]);
 		
        if ($validator->fails()) {
            return redirect()->route("admin.publication.edit",["id"=>$data["id"]])->withErrors($validator)->withInput();
        }
        if (!$data['personal_check'] || $data['personal_check'] == 0)
        	$data['personal_id'] = null;
        
        if ($data['foreing_check'] && $data['foreing_check'] == 1) {
        	$validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
	        ]);
	 		
	        if ($validator->fails()) {
	            return redirect()->route("admin.publication.edit",["id"=>$data["id"]])->withErrors($validator)->withInput();
	        }
        	$author = ucfirst($data['first_name']).' '.ucfirst($data['last_name']);
        }
        else
        	$author = null;
        $publication = Publication::find($data['id']);
        Publication::find($data['id'])->update([
        	'title' => $data['title'],
            'personal_id' => $data['personal_id'],
            'author' => $author,
        ]);

        if (isset($request->file)) {
        	Storage::disk('uploads')->delete('assets/files/publications/'.$publication->file);
            $file_name = "CIF_".$publication->id."_".strtotime('now').".pdf";
            $request->file->storeAs('assets/files/publications',$file_name,'uploads');
            Publication::find($publication->id)->update(["file" => $file_name]);
        }

        return redirect()->route("admin.publication")->with(["message_success"=>"Publicación actualizada exitosamente."]);
    }

    public function delete(Request $request)
    {
        $publication_id=$request->get("id");
        $current_user=User::getCurrent();
        $publication = Publication::find($publication_id);
        Publication::find($publication_id)->delete();

        if (isset($publication->file) && file_exists('assets/files/publications/'.$publication->file)) {
            Storage::disk('uploads')->delete('assets/files/publications/'.$publication->file);
        }
        return redirect()->route("admin.publication")->with(["message_success"=>"Publicación eliminado exitosamente"]);
    }
}
