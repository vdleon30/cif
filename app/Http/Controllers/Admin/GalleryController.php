<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Models\Gallery;
use App\Models\GalleryPhotos;

class GalleryController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "gallery");
    }

    public function index(){
        $user = User::getCurrent();

        return view("admin.gallery.list",["breadcrumb"=>true]);
    }

    public function getList(){
        $gallerys = Gallery::get(); 
        $user = User::getCurrent();
        
        $gallerys_list=[];
       
        foreach($gallerys as $gallery){
            if ($user->hasRole('admin')) {
                $gallerys_list[] = [$gallery->title,$gallery->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.gallery.edit",["id"=>$gallery->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$gallery->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$gallery->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>' ];
            }else{
               $gallerys_list[] = [$gallery->title,$gallery->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.gallery.edit",["id"=>$gallery->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>' ];
            }
    		
        }
        return response()->json(['data' => $gallerys_list]);
    }

    public function getCreate()
    {
        return view("admin.gallery.create",["breadcrumb"=>true]);
    }

    public function create(Request $request)
    {
    	$data = $request->only(['title', 'description']);
    	try {
    		$gallery = Gallery::create([
	    		'title' => ucfirst($data["title"]),
	    		'description' => $data["description"]
	    	]);

	        return redirect()->route("admin.gallery.edit",["id"=>$gallery->id])->with(["message_success"=>"Galeria creada exitosamente."]);
    	} catch (Exception $e) {
    		\Log::info($e);
    		return redirect()->back()->with(["message_error"=>"Ha ocurrido un error, ¡Vuelva a intentar!."]);
    	}
    	
    }

    public function getEdit($id)
    {
    	$gallery = Gallery::find($id);

        return view("admin.gallery.edit",["breadcrumb"=>true,"gallery"=>$gallery]);
    }

    public function update(Request $request)
    {
    	$data = $request->only(['id','title', 'description']);

        $gallery = Gallery::find($data['id']);

        if (!$gallery) {
            return redirect()->route("admin.gallery")->with(["message_error"=>"Ha ocurrido un error, ¡Vuelva a intentar!."]);
        }
        Gallery::find($data['id'])->update([
            'title' => ucfirst($data["title"]),
            'description' => $data["description"]
        ]);

        return redirect()->route("admin.gallery")->with(["message_success"=>"Galeria creada exitosamente."]);
    }

    public function postUpload(Request $request){
    	$data = $request->all();

    	$gallery = Gallery::find($data["id"]);
    	if (!$gallery) {
        	return Response::json('error', 400);
    	}
		
		$rules = array(
		    'file' => 'image|max:3000',
		);

		$validation = Validator::make($data, $rules);

		if ($validation->fails())
		{
			return Response::make($validation->errors->first(), 400);
		}

		$file =$data['file'];

		$order = $gallery->photos->count() + 1;

        $file_name= "CIF_".$gallery->id."_".strtotime('now')."_".$order.".png";

        $d = $request->file->storeAs('assets/files/gallery/img/'.$gallery->id,$file_name,'uploads');

        GalleryPhotos::create([
        	'gallery_id' => $gallery->id,
        	'order' => $order,
        	'file_name' => $file_name
        ]);

        if( $d ) {
        	return response()->json('success', 200);
        } else {
        	return response()->json('error', 400);
        }
	}

    public function getPhotos()
    {
    	$gallery = Gallery::find($_POST["id"]);

    	$result ='<div class="con-photo mt-4"><div class="row text-center text-lg-left">';

        foreach($gallery->photos as $photo){
            $result = $result.' <div class="col-lg-4 col-md-4 col-6" id="imagen-container-'.$photo->id.'"><button value="" type="button" id="'.$photo->id.'" class="btn btn-danger" data-placement="left" title="Eliminar Foto" style="position: absolute;"><i class="fa fa-trash text-right" aria-hidden="true" ></i></button><a href="#" id="imagen-'.$photo->id.'" class="d-block mb-4 h-100"><img class="img-fluid img-thumbnail" src="'.asset("assets/files/gallery/img/".$gallery->id."/".$photo->file_name).'" alt="" style="width: 200px; height:250px; overflow:hidden"></a></div>';
        }
        $result = $result.'</div> </div>';

        return $result;

    }

    public function deletePhotos()
    {
    	try {
    		$deletePhoto =  GalleryPhotos::find($_POST["photo_id"]);
	        GalleryPhotos::find($_POST["photo_id"])->delete();
	    	Storage::disk('uploads')->delete('/assets/files/gallery/img/'.$deletePhoto->gallery_id.'/'.$deletePhoto->file_name);
	    	return response()->json('success', 200);
    	} catch (Exception $e) {
    			return response()->json('error', 400);
    	}
    }

    public function delete(Request $request){
        $gallery_id=$request->get("id");
        $current_user=User::getCurrent();

        $gallery = Gallery::find($gallery_id);

        if (!$gallery) {
            return redirect()->route("admin.gallery")->with(["message_error"=>"Ha ocurrido un error, ¡Vuelva a intentar!."]);
        }
        if ($gallery->photos->isNotEmpty()) {
            foreach ($gallery->photos as $key => $photo) {
                Storage::disk('uploads')->delete('/assets/files/gallery/img/'.$gallery->id.'/'.$photo->file_name);
                GalleryPhotos::find($photo->id)->delete();
            }
        }
        
        rmdir(public_path().'/assets/files/gallery/img/'.$gallery->id);
        Gallery::where("id",$gallery_id)->delete();

        return redirect()->route("admin.gallery")->with(["message_success"=>"Galería eliminada exitosamente"]);
    }
}
