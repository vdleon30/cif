<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "users");
    }

    public function index(){
        $user = User::getCurrent();

        return view("admin.users.list",["breadcrumb"=>true]);
    }

    public function getList(){
        $current_user = User::getCurrent();

        $users = User::get(); 
        
        $users_list=[];
       
        foreach($users as $user){
        	$roles="";

            foreach($user->roles as $k=>$user_rol){
                $roles.="<span>".($user_rol->display_name)."</span>";
                if($k+1!=count($user->roles)){
                    $roles.=" ";
                }
            }
            $botom = '<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.users.edit",["id"=>$user->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>';

            if ($current_user->hasRole("admin")) {
                $botom = '<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.users.edit",["id"=>$user->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$user->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$user->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>';
            }

            if ($user->id == $current_user->id) {
                $botom = '';
                # code...
            }

    		$users_list[] = [$user->email."<span>".$user->first_name." ".$user->last_name."</span>",$roles,$user->created_at->format("Y/m/d"),$botom
            ];
        }
        return response()->json(['data' => $users_list]);
    }
    public function getCreate()
    {
        $user = User::getCurrent();
        $roles = Role::get();

        return view("admin.users.create",["breadcrumb"=>true,"roles"=>$roles]);
    }

    public function create(Request $request)
    {
        $data=$request->only(["first_name","last_name","mobile_number","dni","username","email","rol","password","password_confirmation"]);

        $current_user = User::getCurrent();
        
       
        $validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'mobile_number' => ['numeric'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'dni' => ['required', 'string','unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'username' => ['required', 'string', 'unique:users'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.users.create")->withErrors($validator)->withInput();
        }

        $user = User::create([
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'dni'   => $data['dni'],
            'username'   => $data['username'],
            'password' => Hash::make($data['password']),
        ]);
        $user->attachRole(Role::where("name",$data["rol"])->first());

        try{
            if(\App::environment('production')){
                \Mail::send('emails.user_welcome', ['user'=>$user,'data'=>$data], function($message) use($user)
                {
                    $message->subject('¡Bienvenido a CIF!');
                    $message->to($user->email,$user->first_name);
                });
            }
        }catch(\Exception $e){
            \Log::error('Cant send email: '.$e->getMessage());
            return redirect()->back()->withErrors('Error, por favor intenta mas tarde.');
        }

        return redirect()->route("admin.users")->with(["message_success"=>"Usuario creado exitosamente."]);
    }

    public function getEdit($id){
        $user = User::getCurrent();
        $edit_user = User::find($id);
        $roles = Role::get();
        return view("admin.users.edit",["user"=>$user,"edit_user"=>$edit_user,"roles"=>$roles,"breadcrumb"=>true]);
    }

    public function update(Request $request)
    {
        $data=$request->only(["id","first_name","last_name","mobile_number","dni","email","username","password","password_confirmation","rol"]);

        $user = User::find($data["id"]);
        $current_user = User::getCurrent();
        if(isset($data["rol"]) && $user->rol != $data["rol"]){
            $user->detachRoles($user->rol);
            $user->attachRole(Role::where("name",$data["rol"])->first());
        }
        $user = User::find($data["id"]);

        $password=$request->get("password");
        if($password && !empty($password)){
            $confirm_password=$request->get("password_confirmation");

            if($password!=$confirm_password){
                return redirect()->back()->withErrors(["Las contraseñas no coinciden"]);
            }

            if(strlen($password)<6){
                return redirect()->back()->withErrors(["La contraseña debe tener al menos 6 caracteres"]);
            }

            User::where("id",$user->id)->update(["password"=>Hash::make($password)]);
        }

     
        $validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'mobile_number' => ['numeric'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'username' => ['required', 'string', 'unique:users,username,'.$user->id],
            'dni' => ['required', 'string', 'unique:users,dni,'.$user->id],
        ]);

        if ($validator->fails()) {
            return redirect()->route("admin.users.edit",["id"=>$user->id])->withErrors($validator)->withInput();
        }

        User::where("id",$user->id)->update([
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'username' => $data['username'],
            'dni' => $data['dni'],
        ]);

        return redirect()->route("admin.users.edit",["id"=>$data["id"]])->with(["message_success"=>"Usuario actualizado exitosamente."]);
    }
    public function delete(Request $request){
        $user_id=$request->get("id");
        $current_user=User::getCurrent();
        $user = User::where("id",$user_id)->delete();

        return redirect()->route("admin.users")->with(["message_success"=>"Usuario eliminado exitosamente"]);
    }
}
