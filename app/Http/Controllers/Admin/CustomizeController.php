<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Customize;

class CustomizeController extends Controller
{
	 public function __construct()
    {
        View::share('menu_active', "customize");
    }

    public function index()
    {
        return view("admin.customize.index",["breadcrumb"=>true]);
    }

    public function change($type)
    {
        $item = null;
        if ($type == 'descripcion') {
            $items = Customize::where("type","descripcion")->first();
        }

        if ($type == 'gestion') {
            $items = Customize::where("type","gestion")->get();
        }

        if ($type == 'link') {
            $items = Customize::where("type","link_group")->get();
            foreach ($items as $key => $value) {
            	$value->links = Customize::where("type","link_".$value->data_x)->get();
            }
        }

        return view('admin.customize.change',["type"=>$type,"items"=>$items]);
    }

    public function saveDescription(Request $request)
    {
    	$item = Customize::where("type","descripcion")->first();
        if ($item) {
            Customize::where("type","descripcion")->update([
                'data_y' => $request->description
            ]);
        }else{
            Customize::create([
                'type' => "descripcion",
                'data_y' => $request->description
            ]);
        }

        return redirect()->route('admin.customize')->with(["message_success"=>"Actualizado exitosamente."]);
    }

    public function saveGestion(Request $request)
    {
    	if (isset($request->title)) {
	    	foreach ($request->title as $key => $value) {
	    		$id = 0;

	    		if (isset($request->id[$key])) {
	    			$id = $request->id[$key];
	    		}
	    		$item = Customize::find($id);
		        if ($item) {
		            Customize::find($id)->update([
		                'data_y' => $request->description[$key],
		                'data_x' => $value
		            ]);
		        }else{
		            Customize::create([
		                'type' => "gestion",
		                'data_x' => $value,
		                'data_y' => $request->description[$key]
		            ]);
		        }
	    	}
    	}

    	if (isset($request->delete)) {
    		foreach ($request->delete as $key => $value) {
	    		$item = Customize::find($value)->delete();
	    	}
    	}

    	

        return redirect()->route('admin.customize')->with(["message_success"=>"Actualizado exitosamente."]);
    }

     public function saveLink(Request $request)
    {
    	if (isset($request->title_group)) {
	    	foreach ($request->title_group as $key => $element) {
	    		$check_group = Customize::where("type", "link_group")->where("data_x", $element)->first();
	    		if (!$check_group) {
					$check_gruop = Customize::create([
		                'type' => "link_group",
		                'data_x' => $element,
		            ]);
	    		}
                if (isset($request->title_link[$key])) {
    	    		foreach ($request->title_link[$key] as $k => $value) {
    	    			if (isset($request->link[$key][$k]) && $value ) {
                            $check_link = null;
                            if (isset($request->id[$key][$k] )) {
                                $check_link = Customize::where("id", $request->id[$key][$k])->first();
                            }
    	    				if (!$check_link) {
    	    					Customize::create([
    				                'type' => "link_".$element,
    				                'data_x' => $value,
    				                'data_y' => $request->link[$key][$k]
    				            ]);
    	    				}else{
    	    					Customize::where("id", $request->id[$key][$k])->update([
    				                'type' => "link_".$element,
    				                'data_x' => $value,
    				                'data_y' => $request->link[$key][$k]
    				            ]);
    	    				}
    	    				
    	    			}
                    }
	    		}
	    	}
    	}

    	if (isset($request->delete)) {
    		foreach ($request->delete as $key => $value) {
	    		$item = Customize::find($value);
                if ($item && $item->type == "link_group") {
                    Customize::where("type","link_".$item->data_x)->delete();
                    $item->delete();
                }elseif($item)
                    $item->delete();
	    	}
    	}
    	
    	

        return redirect()->route('admin.customize')->with(["message_success"=>"Actualizado exitosamente."]);
    }
}
