<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Events;

class EventsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "events");
    }

    public function index(){
        $user = User::getCurrent();
        

        return view("admin.events.list",["breadcrumb"=>true]);
    }

    public function getList(){
        $events = Events::get(); 
        $user = User::getCurrent();
        $events_list=[];
       
        foreach($events as $event){
        	$hour = \DateTime::createFromFormat("H:i",$event->hour);
            if ($user->hasRole('admin')) {
                $events_list[] = [$event->title,$event->date."<span>".$hour->format('g:ia')."</span>",$event->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.events.edit",["id"=>$event->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br><a id="'.$event->id.'" class="btn btn-sm btn-outline-danger mb-1 " data-toggle="modal" data-target="#myModal" data-whatever="aceptar/'.$event->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'];
            }else{
                $events_list[] = [$event->title,$event->date."<span>".$hour->format('g:ia')."</span>",$event->created_at->format("Y/m/d"),'<div class="container-btn--action-table"><a class="btn btn-sm btn-outline-info mb-1" href="'.route("admin.events.edit",["id"=>$event->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a><br></div>'];

            }
    		
        }
        return response()->json(['data' => $events_list]);
    }

    public function getCreate()
    {
        return view("admin.events.create",["breadcrumb"=>true]);
    }

    public function create(Request $request)
    {
    	$data = $request->only(["title","description","date","hour","place"]);
    	$validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'date' => ['required','string'],
            'hour' => ['required', 'string'],
            'place' => ['required', 'string'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.events.create")->withErrors($validator)->withInput();
        }
        $event = Events::create([
        	'title' => $data['title'],
            'description' => $data['description'],
            'hour' => $data['hour'],
            'date' => $data['date'],
            'place' => $data['place'],
        ]);
        $type = $_FILES['photo']['type'];

        if (isset($request->photo)) {
            if (strpos($type, 'pdf')) {
                $file_name = "CIF_".$event->id."_".strtotime('now').".pdf";
            }else{
                $file_name = "CIF_".$event->id."_".strtotime('now').".png";
            }
        	$request->photo->storeAs('assets/files/events/img',$file_name,'uploads');
        	Events::find($event->id)->update(["photo" => $file_name]);
        }

        return redirect()->route("admin.events")->with(["message_success"=>"Evento creado exitosamente."]);
    }

    public function getEdit($id){
        $user = User::getCurrent();
        $event = Events::find($id);
        return view("admin.events.edit",["user"=>$user,"event"=>$event,"breadcrumb"=>true]);
    }

    public function update(Request $request)
    {
        $data = $request->only(["id","title","description","date","hour","place"]);
        $event = Events::find($data["id"]);
        if (!$event) {
            return redirect()->route("admin.events")->with(["message_error"=>"Evento no se encuentra disponible."]);
        }
        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'date' => ['required','string'],
            'hour' => ['required', 'string'],
            'place' => ['required', 'string'],
        ]);
 
        if ($validator->fails()) {
            return redirect()->route("admin.events.edit",["id"=>$data["id"]])->withErrors($validator)->withInput();
        }

        Events::find($data["id"])->update([
            'title' => $data['title'],
            'description' => $data['description'],
            'hour' => $data['hour'],
            'date' => $data['date'],
            'place' => $data['place'],
        ]);

        $event = Events::find($data["id"]);

        if (isset($request->photo)) {
            if ($event->photo) {
                Storage::disk('uploads')->delete('/assets/files/events/img/'.$event->photo);
            }
            $type = $_FILES['photo']['type'];
            if (strpos($type, 'pdf')) {
                $file_name = "CIF_".$event->id."_".strtotime('now').".pdf";
            }else{
                $file_name = "CIF_".$event->id."_".strtotime('now').".png";

            }
            $request->photo->storeAs('assets/files/events/img',$file_name,'uploads');
            Events::find($event->id)->update(["photo" => $file_name]);
        }

        return redirect()->route("admin.events.edit",["id"=>$data["id"]])->with(["message_success"=>"Evento actualizado exitosamente."]);
    }

    public function delete(Request $request)
    {
        $event_id=$request->get("id");
        $current_user=User::getCurrent();
        $event = Events::where("id",$event_id);
        Events::where("id",$event_id)->delete();

         if (isset($event->photo) && file_exists('assets/files/events/img/'.$event->photo)) {
            Storage::disk('uploads')->delete('assets/files/events/img/'.$event->photo);
        }

        return redirect()->route("admin.events")->with(["message_success"=>"Evento eliminado exitosamente"]);
    }
}
