<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Publication;

class PublicationController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "publications");
    }
    
    public function index()
    { 
    	$publications = Publication::all();
        return view('platform.publications.publication',["publications"=>$publications]);
    }

    public function download($id)
    {
    	try {
    		$publication = Publication::find($id);
        	return response()->download(public_path().'/assets/files/publications/'.$publication->file);
    	} catch (\Exception $e) {
    		
    	}
    }

    public function getPreview()
    {
    	$id = $_POST["id"];

    	$publication = Publication::find($id);

    	return 'assets/files/publications/'.$publication->file;
    }
}
