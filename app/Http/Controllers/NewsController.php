<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\News;

class NewsController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "news");
    }
    public function index(Request $request)
    { 
    	$year = '2019';
    	if ($request->year) {
    		$year = $request->get('year');
    	}
        $news = News::where("date","like",$year.'%')->orderBy("date","DESC")->paginate(12);
        return view('platform.news.news',["news"=>$news,"year"=>$year]);
    }
}
