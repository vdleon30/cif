<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Events;
use App\Models\News;
use App\Models\Customize;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check() &&  (Auth::user()->hasRole(["admin"] || Auth::user()->hasRole(["assistant"]) ))){
            return redirect()->route("admin.dashboard");
        }
        return redirect()->route("dashboard");

        return view('home');
    }

    public function dashboard()
    {
        if(Auth::check() &&  (Auth::user()->hasRole(["admin"]) || Auth::user()->hasRole(["assistant"]) )  ){
            return redirect()->route("admin.dashboard");
        }
        $events = Events::orderBy("date","DESC")->get();
        $news = News::orderBy("date","DESC")->get();
        return view('main.dashboard',["events"=>$events,"news"=>$news]);
    }

    public function contact_us()
    {
        return view('platform.platform',["menu_active"=>'about_us',"sub_menu_active"=>'contact_us']);
    }

    public function about_us()
    {
        $description = Customize::where("type","descripcion")->first();
        if ($description) {
            $description = $description->data_y;
        }
        $gestion =  Customize::where("type","gestion")->get();
        return view('platform.platform',["menu_active"=>'about_us',"sub_menu_active"=>'about_us_info',"description"=>$description,"gestion"=>$gestion]);
    }
    public function about_usType($type)
    {
        if ($type == "enlaces_de_interes") {
            $items = Customize::where("type","link_group")->get();
            foreach ($items as $key => $value) {
                $value->links = Customize::where("type","link_".$value->data_x)->get();
            }
            return view('platform.platform',["menu_active"=>'about_us',"sub_menu_active"=>'link',"items"=>$items]);
        }
    }
}
