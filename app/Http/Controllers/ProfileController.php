<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

class ProfileController extends Controller
{
   public function index()
   {
   		$user = User::getCurrent();

        return view('platform.user.profile',["user"=>$user]);

   }

   public function updateProfile(Request $request)
   {
   	 	$data=$request->only(["first_name","last_name","mobile_number","dni","email","username","password","password_confirmation"]);

        $user = User::getCurrent();

        $password=$request->get("password");
        if($password && !empty($password)){
            $confirm_password=$request->get("password_confirmation");

            if($password!=$confirm_password){
                return redirect()->back()->withErrors(["Las contraseñas no coinciden"]);
            }

            if(strlen($password)<6){
                return redirect()->back()->withErrors(["La contraseña debe tener al menos 6 caracteres"]);
            }

            User::where("id",$user->id)->update(["password"=>Hash::make($password)]);
        }

     
        $validator = Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'mobile_number' => ['numeric'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'username' => ['required', 'string', 'unique:users,username,'.$user->id],
            'dni' => ['required', 'string', 'unique:users,dni,'.$user->id],
        ]);

        if ($validator->fails()) {
            return redirect()->route("profile")->withErrors($validator)->withInput();
        }

        User::where("id",$user->id)->update([
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'mobile_number' => $data['mobile_number'],
            'email' => $data['email'],
            'username' => $data['username'],
            'dni' => $data['dni'],
        ]);

        return redirect()->route("profile")->with(["message_success"=>"Perfil actualizado exitosamente."]);
   }
}
