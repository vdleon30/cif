<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Personal;
use App\Models\PersonalJob;

class PersonalController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "personal");
    }
    public function index()
    {
    	$personals = Personal::all();
    	$jobs = PersonalJob::all();
        return view('platform.personal.personal',["personals" => $personals,"jobs" => $jobs]);
    }

    public function viewPersonal($id)
    {
        $personal = Personal::find($id);
        return view('platform.personal.single_personal',["personal" => $personal]);
    	
    }
}
