<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Gallery;

class GalleryController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "gallerys");
    }
    public function index()
    { 
        $gallerys = Gallery::paginate(12);
        return view('platform.gallerys.index',["gallerys"=>$gallerys]);
    }

    public function getCarousel()
    {
    	$gallery = Gallery::find($_POST["id"]);
    	$result = '';
    	if ($gallery && $gallery->photos->isNotEmpty()) {
    		$result = '<div id="carouselExampleControls" class="carousel slide" data-ride="carousel"><div class="carousel-inner">';
    		foreach ($gallery->photos as $key => $photo) {
    			$file = asset('assets/files/gallery/img/'.$gallery->id.'/'.$photo->file_name);
    			if ($key == 0) {
    				$result = $result .'<div class="carousel-item active"><img src="'.$file.'" class="d-block w-100" alt="..."></div>';
    				continue;
    			}
    				$result = $result .'<div class="carousel-item"><img src="'.$file.'" class="d-block w-100" alt="..."></div>';

    		}
    		$result = $result .'<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div></div><div id="description"><hr>
                                '.$gallery->description.'</div>';
    	}
    	return $result;
    }
}
