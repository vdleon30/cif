<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Events;

class EventsController extends Controller
{
	public function __construct()
    {
        View::share('menu_active', "events");
    }
    public function index(Request $request)
    { 
    	$year = '2019';
    	if ($request->year) {
    		$year = $request->get('year');
    	}
        $events = Events::where("date","like",$year.'%')->orderBy("date","DESC")->paginate(12);
        return view('platform.events.events',["events"=>$events,"year"=>$year]);
    }
}
