<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->middleware("platform")->name('home');
Route::get('/', 'HomeController@index')->middleware("platform")->name('homes');
Route::get('/', 'HomeController@dashboard')->middleware("platform")->name('dashboard');
Route::get('contactanos', 'HomeController@contact_us')->middleware("platform")->name('contact_us');
Route::get('conocenos', 'HomeController@about_us')->middleware("platform")->name('about_us');
Route::get('conocenos/{type}', 'HomeController@about_usType')->middleware("platform")->name('about_us.type');

Route::get('admin', 'Admin\AdminController@dashboard')->middleware("logged")->name('admin.dashboard');

//ADMIN -- USERS
Route::get('admin/usarios', 'Admin\UserController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users');
Route::get('admin/usarios/get', 'Admin\UserController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.get');
Route::get('admin/usarios/registrar', 'Admin\UserController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.create');
Route::post('admin/usarios/registrar', 'Admin\UserController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.save');
Route::get('admin/usarios/editar/{id}', 'Admin\UserController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.edit');
Route::post('admin/usarios/editar', 'Admin\UserController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.update');
Route::post('admin/usarios/trash', 'Admin\UserController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.users.delete');

//ADMIN -- PERSONAL
Route::get('admin/personal', 'Admin\PersonalController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal');
Route::get('admin/personal/get', 'Admin\PersonalController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.get');
Route::get('admin/personal/registrar', 'Admin\PersonalController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.create');
Route::post('admin/personal/registrar', 'Admin\PersonalController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.save');
Route::get('admin/personal/editar/{id}', 'Admin\PersonalController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.edit');
Route::post('admin/personal/editar', 'Admin\PersonalController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.update');
Route::post('admin/personal/trash', 'Admin\PersonalController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.delete');

Route::get('admin/personal/cargos', 'Admin\PersonalController@indexJob')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.job');
Route::get('admin/personal/cargos/get', 'Admin\PersonalController@getListJob')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.job.get');
Route::post('admin/personal/cargos/save', 'Admin\PersonalController@createJob')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.job.save');
Route::post('admin/personal/cargos/update', 'Admin\PersonalController@updateJob')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.job.update');
Route::post('admin/personal/cargos/delete', 'Admin\PersonalController@deleteJob')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.personal.job.delete');


//ADMIN -- CUSTOMIZE
Route::get('admin/personalizar', 'Admin\CustomizeController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.customize');
Route::get('admin/personalizar/{type}', 'Admin\CustomizeController@change')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.customize.change');
Route::post('admin/personalizar/description/save', 'Admin\CustomizeController@saveDescription')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.customize.description.save');
Route::post('admin/personalizar/gestion/save', 'Admin\CustomizeController@saveGestion')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.customize.gestion.save');
Route::post('admin/personalizar/link/save', 'Admin\CustomizeController@saveLink')->middleware("auth")->middleware("logged")->middleware(['role:admin'])->name('admin.customize.link.save');


//ADMIN -- EVENTS
Route::get('admin/eventos', 'Admin\EventsController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events');
Route::get('admin/eventos/get', 'Admin\EventsController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.get');
Route::get('admin/eventos/registrar', 'Admin\EventsController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.create');
Route::post('admin/eventos/registrar', 'Admin\EventsController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.save');
Route::get('admin/eventos/editar/{id}', 'Admin\EventsController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.edit');
Route::post('admin/eventos/editar', 'Admin\EventsController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.update');
Route::post('admin/eventos/trash', 'Admin\EventsController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.events.delete');


//ADMIN -- NEWS
Route::get('admin/noticias', 'Admin\NewsController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news');
Route::get('admin/noticias/get', 'Admin\NewsController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.get');
Route::get('admin/noticias/registrar', 'Admin\NewsController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.create');
Route::post('admin/noticias/registrar', 'Admin\NewsController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.save');
Route::get('admin/noticias/editar/{id}', 'Admin\NewsController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.edit');
Route::post('admin/noticias/editar', 'Admin\NewsController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.update');
Route::post('admin/noticias/trash', 'Admin\NewsController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.news.delete');

//ADMIN -- PUBLICATIONS
Route::get('admin/publicaciones', 'Admin\PublicationController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication');
Route::get('admin/publicaciones/get', 'Admin\PublicationController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.get');
Route::get('admin/publicaciones/registrar', 'Admin\PublicationController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.create');
Route::post('admin/publicaciones/registrar', 'Admin\PublicationController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.save');
Route::get('admin/publicaciones/editar/{id}', 'Admin\PublicationController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.edit');
Route::post('admin/publicaciones/editar', 'Admin\PublicationController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.update');
Route::post('admin/publicaciones/trash', 'Admin\PublicationController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.publication.delete');



//ADMIN -- GALLERY
Route::get('admin/galerias', 'Admin\GalleryController@index')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery');
Route::get('admin/galerias/get', 'Admin\GalleryController@getList')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.get');
Route::get('admin/galerias/registrar', 'Admin\GalleryController@getCreate')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.create');
Route::post('admin/galerias/registrar', 'Admin\GalleryController@create')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.save');
Route::get('admin/galerias/editar/{id}', 'Admin\GalleryController@getEdit')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.edit');
Route::post('admin/galerias/editar', 'Admin\GalleryController@update')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.update');
Route::post('admin/galerias/photos', 'Admin\GalleryController@postUpload')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.photos');
Route::post('admin/galerias/photos/get', 'Admin\GalleryController@getPhotos')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.get.photo');
Route::post('admin/galerias/photos/delete', 'Admin\GalleryController@deletePhotos')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.delete.photo');
Route::post('admin/galerias/trash', 'Admin\GalleryController@delete')->middleware("auth")->middleware("logged")->middleware(['role:admin|assistant'])->name('admin.gallery.delete');




//EVENTS
Route::get('eventos', 'EventsController@index')->middleware("platform")->name('events');


//NEWS
Route::get('noticias', 'NewsController@index')->middleware("platform")->name('news');

//PERSONAL
Route::get('personal', 'PersonalController@index')->middleware("platform")->name('personal');
Route::get('personal/view/{id}', 'PersonalController@viewPersonal')->middleware("platform")->name('view_personal');

//PUBLICATION
Route::get('publicaciones', 'PublicationController@index')->middleware("platform")->name('publications');
Route::get('publicaciones/download/{id}', 'PublicationController@download')->middleware("platform")->name('publications.download');
Route::post('publicaciones/getPreview', 'PublicationController@getPreview')->middleware("platform")->name('publications.preview');

//GALerias
Route::get('galerias', 'GalleryController@index')->middleware("platform")->name('gallery');
Route::post('galerias/carousel', 'GalleryController@getCarousel')->middleware("platform")->name('gallery.carousel');

//PROFILE
Route::get('profile', 'ProfileController@index')->name('profile');
Route::post('profile/update', 'ProfileController@updateProfile')->name('profile.update');
